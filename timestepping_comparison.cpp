// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "helper_mkl.hpp"
#include "helper_comparison.hpp"

namespace mlhp
{
struct Compute : public Comparison
{   
    using Comparison::Comparison;

    template<typename MatrixType>
    auto compute( bool pardiso ) 
    {
        size_t nsteps = 128;
        double theta = 0.5;

        auto initialCondition = spatial::constantFunction<D>( initialTemperature );

        auto makeBasis = [this](const auto& grid) 
        {
            return useTrunkSpace ? makeHpBasis<TrunkSpace>(grid, degree) :
                                   makeHpBasis<TensorSpace>(grid, degree);
        }; 

        // Initial discretization
        auto grid0 = makeRefinedGrid( nelements, lengths, origin );
        auto grid1 = grid0;

        auto [solver, info] = !pardiso ? makeMklCGSolverWithInfo( 1e-10 ) : std::pair { makePardisoSolver( ), std::make_shared<linalg::IterativeSolverInfo>( ) }; 

        auto basis0 = makeBasis( grid0 );
        auto dofs0 = projectOnto( *basis0, initialCondition, solver );

        size_t nprojection = 1, itprojection = info->niterations( );
        size_t nnewton = 0, itnewton = 0;
        // std::vector<std::array<size_t, 2>> it;

        // // Postprocess initial condition
        // std::vector postprocessors =
        // {
        //     makeSolutionPostprocessor<D>( dofs0, 1 ),
        //     makeFunctionPostprocessor<D>( spatial::sliceLast( source, 0.0 ), "Source" ),
        //     makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements, 0.0 )
        // };
        // 
        // auto resolution = array::make<size_t, D>( degree );
        // auto filename = "timestepping_comparison_";
        // 
        // postprocess( resolution, postprocessors, *basis0, filename + std::to_string( 0 ), 1 );
        // 
        // size_t vtuInterval = 32;

        // Assembly stuff

        double dt = duration / nsteps;

        size_t ndof = 0;

        // Time stepping
        for( size_t iTimeStep = 0; iTimeStep < nsteps; ++iTimeStep )
        {
            double time0 = iTimeStep * dt;
            double time1 = ( iTimeStep + 1 ) * dt;

            // Create discetisation for t^{i+1}
            grid1 = makeRefinedGrid( nelements, lengths, origin );
            grid1->refine( laser::makeRefinement<D>( laserTrack, refinements, time1 ) );
            
            auto basis1 = makeBasis( grid1 );

            // std::cout << "Time step " << iTimeStep + 1 << " / " << nsteps;
            // std::cout << " (" << basis1->ndof( ) << " number of unknowns)" << std::endl;

            auto df = allocateMatrix<MatrixType>( *basis1 );

            std::vector<double> dofs1 = projectOnto( *basis0, *basis1, dofs0, df, solver );
            std::vector<double> f( df.size1( ), 0.0 );
            
            nprojection++;
            itprojection += info->niterations( );

            double norm0 = 0.0;

            // Newton-Raphson iterations
            for( size_t i = 0; i < 40; ++i )
            {
                std::fill( f.begin( ), f.end( ), 0.0 );
                std::fill( df.data( ), df.data( ) + df.nnz( ), 0.0 );

                auto integrand = makeTimeSteppingThermalIntegrand<3>( material, phaseChange,
                    source, dofs0, dofs1, std::array{ time0, time1 }, theta );

                auto quadrature = makeIntegrationOrderDeterminor<D>( 2 ); // p + 2

                integrateOnDomain( *basis0, *basis1, integrand, { df, f }, quadrature );

                double norm1 = std::sqrt( std::inner_product( f.begin( ), f.end( ), f.begin( ), 0.0 ) );

                norm0 = i == 0 ? norm1 : norm0;

                // std::cout << "iteration " << i + 1 << ": | F | = " << norm1 / norm0 << " (" << norm1 << ")" << std::endl;
                
                //auto t0 = utilities::tic( );
                auto dx = solver( df, f );

                nnewton++;
                itnewton += info->niterations( );

                // if( i >= it.size( ) ) it.resize(i + 1);
                //
                // it[i][0] += info->niterations();
                // it[i][1]++;

                //utilities::toc( t0, "Solving took: " );

                std::transform( dofs1.begin( ), dofs1.end( ), dx.begin( ), dofs1.begin( ), std::minus<double> { } );

                if( norm1 / norm0 <= 1e-10 || norm1 < 1e-10 ) break;
            }

            // // Postprocess vtu file with 6 sub-cells per element
            // std::vector postprocessors =
            // {
            //     makeSolutionPostprocessor<D>( dofs1, 1 ),
            //     makeFunctionPostprocessor<D>( spatial::sliceLast( source, time1 ), "Source" ),
            //     makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements, time1 )
            // 
            // };
            // 
            // if( ( iTimeStep + 1 ) % vtuInterval == 0 )
            // {
            //     postprocess( resolution, postprocessors, *basis1, filename +
            //         std::to_string( ( iTimeStep / vtuInterval ) + 1 ), 1 );
            // }
             
            ndof += dofs1.size( );

            // Move discretization for next step
            dofs0 = std::move( dofs1 );
            grid0 = std::move( grid1 );
            basis0 = std::move( basis1 );
        }

        // std::cout << "Average number of cg iterations per newton iteration: ";
        //
        // for( auto entry : it ) std::cout << entry[0] / static_cast<double>( entry[1] ) << " ";
        // std::cout << std::endl;

        return std::tuple { ndof, itprojection, nprojection, itnewton, nnewton };
    }
};

} // namespace mlhp

int main( int argc, char** argv )
{
    MLHP_CHECK( argc == 6, "Usage: timestepping_comparison [serial|parallel] [cg|pardiso] [symmetric|unsymmetric] beginStudy endStudy" )

    bool serial = argv[1] == std::string { "serial" };
    bool pardiso = argv[2] == std::string { "pardiso" };
    bool symmetric = argv[3] == std::string { "symmetric" };

    size_t beginStudy = std::stoul( argv[4] );
    size_t endStudy = std::stoul( argv[5] );
    
    MLHP_CHECK( serial || argv[1] == std::string { "parallel" }, "First argument must be serial or parallel." );
    MLHP_CHECK( pardiso || argv[2] == std::string { "cg" }, "Second argument must be cg or pardiso." );
    MLHP_CHECK( symmetric || argv[3] == std::string { "unsymmetric" }, "Third argument must be symmetric or unsymmetric." );

    std::cout << "Configuration: " << ( serial ? "serial" : "parallel" ) << ( pardiso ? ", pardiso, " : ", cg, " ) << 
        ( symmetric ? "symmetric" : "unsymmetric" ) << ", beginStudy: " << beginStudy << ", endStudy: " << endStudy << std::endl;

    for( size_t i = beginStudy; i <= endStudy; ++i )
    {
        auto t0 = mlhp::utilities::tic( );
   
        auto [ndof, itprojection, nprojection, itnewton, nnewton] = symmetric ? 
            mlhp::Compute { serial, i }.compute<mlhp::linalg::SymmetricSparseMatrix>( pardiso ) : 
            mlhp::Compute { serial, i }.compute<mlhp::linalg::UnsymmetricSparseMatrix>( pardiso );

        std::string it = pardiso ? "" : ", " + std::to_string(itprojection) + ", " + std::to_string(nprojection) + ", " + std::to_string(itnewton) + ", " + std::to_string(nnewton);

        mlhp::utilities::toc( t0, std::to_string( i ) + ", " + std::to_string( ndof / 128.0 ) + ", ", it + "\n" );
    }
}
