#include "mlhp/core.hpp"

namespace mlhp
{

template<size_t D>
auto makeBasis( const HierarchicalGridSharedPtr<D>& grid,
                std::string gradingString,
                std::string basisString,
                size_t refinementLevel )
{
    MLHP_CHECK( basisString == "trunk" || basisString == "tensor", "Invalid basis string." );
    MLHP_CHECK( gradingString == "graded" || gradingString == "uniform", "Invalid grading string." );

    PolynomialDegreeDistributor<D> grading;
    
    if( gradingString == "graded" )
    {
        grading = LinearGrading { 1 };
    }
    else
    {
        grading = UniformGrading { refinementLevel + 1 };
    }

    if( basisString == "trunk" )
    {
        return makeHpBasis<TrunkSpace>( grid, grading );
    }
    else
    {
        return makeHpBasis<TensorSpace>( grid, grading );
    }
}

template<size_t D>
auto compute( size_t refinementLevel, std::string degreesString, std::string basisString, bool writeVtu )
{
    auto t0 = utilities::tic( );

    auto circle = implicit::sphere<D>( array::make<double, D>( 0.0 ), 1e-10 );

    auto numberOfBaseElements = array::make<size_t, D>( 2 );
    auto lengths = array::make<double, D>( 1.0 );

    std::function kappa = []( std::array<double, D> ) noexcept { return 1.0; };

    // Create and refine grid
    auto grid = makeRefinedGrid( numberOfBaseElements, lengths );

    grid->refine( refineTowardsDomainBoundary( circle, refinementLevel ) );

    auto t1 = utilities::tic( );

    // Create mlhp basis on refined grid
    auto basis = makeBasis( grid, degreesString, basisString, refinementLevel );

    auto t2 = utilities::tic( );

    // Boundary conditions
    std::vector<size_t> faces;

    for( size_t axis = 0; axis < D; ++axis )
    {
        faces.push_back( boundary::face( axis, 1 ) );
    }

    auto solution = solution::singularSolution<D>( );
    auto solutionDerivatives = solution::singularSolutionDerivatives<D>( );
    auto source = solution::singularSolutionSource<D>( );

    auto boundaryDofs = boundary::boundaryDofs<D>( solution, *basis, faces );

    auto t3 = utilities::tic( );

    // Allocate sparse matrix
    auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, boundaryDofs.first );
    auto vector = std::vector<double>( matrix.size1( ), 0.0 );

    auto integrand = makePoissonIntegrand( kappa, source );

    auto t4 = utilities::tic( );

    integrateOnDomain( *basis, integrand, { matrix, vector }, boundaryDofs );

    auto t5 = utilities::tic( );

    auto multiply = linalg::makeDefaultMultiply( matrix );
    auto preconditioner = linalg::makeDiagonalPreconditioner( matrix );
    auto interiorDofs = std::vector<double>( matrix.size1( ) );

    auto norms = linalg::cg( multiply, vector, interiorDofs, preconditioner, matrix.size1( ), 1e-10 );

    auto fullSolution = boundary::inflate( interiorDofs, boundaryDofs );

    auto t6 = utilities::tic( );

    auto l2ErrorIntegrand = makeL2ErrorIntegrand<D>( fullSolution, solution );
    auto energyErrorIntegrand = makeEnergyErrorIntegrand<D>( fullSolution, solutionDerivatives );

    auto l2Errors = ErrorIntegrals { };
    auto energyErrors = ErrorIntegrals { };

    integrateOnDomain( *basis, l2ErrorIntegrand, l2Errors, makeIntegrationOrderDeterminor<D>( 3 ) );
    integrateOnDomain( *basis, energyErrorIntegrand, energyErrors, makeIntegrationOrderDeterminor<D>( 3 ) );

    auto t7 = utilities::tic( );

    if constexpr( D <= 3 )
    {
        if( writeVtu )
        {
            auto numberOfResultCells = array::make<size_t, D>( 10 );
            auto filename = "singular_" + std::to_string( D ) + "D_" + std::to_string( refinementLevel ) + ".vtu";

            std::vector postprocessors
            {
                makeSolutionPostprocessor<D>( fullSolution, 1 ),
                makeFunctionPostprocessor<D>( solution, "AnalyticalSolution" ),
                makeFunctionPostprocessor<D>( solutionDerivatives, "AnalyticalDerivatives" ),
                makeFunctionPostprocessor<D>( source, "Source" ),
                makeRefinementLevelPostprocessor( *grid )
            };

            postprocess( numberOfResultCells, postprocessors, *basis, filename);
        }
    }

    auto t8 = utilities::tic( );

    std::cout << interiorDofs.size( )
              << ", " << l2Errors.relativeDifference( )
              << ", " << energyErrors.relativeDifference( )
              << ", " << norms.size( )
              << ", " << matrix.nnz( )
              << ", " << utilities::seconds( t0, t6 )
              << ", " << utilities::seconds( t0, t1 )
              << ", " << utilities::seconds( t1, t2 )
              << ", " << utilities::seconds( t2, t3 )
              << ", " << utilities::seconds( t3, t4 )
              << ", " << utilities::seconds( t4, t5 )
              << ", " << utilities::seconds( t5, t6 )
              << ", " << utilities::seconds( t6, t7 )
              << ", " << utilities::seconds( t7, t8 ) << std::endl;
}

template<size_t D>
void study( std::string degreesString, std::string basisString, size_t beginStudy, size_t endStudy, bool writeVtu )
{
    std::cout << "One dummy computation for more accurate timings afterwards:" << std::endl;
    mlhp::compute<D>( 0, degreesString, basisString, writeVtu );

    std::cout << "\nDofs without boundary, relative errors without percent, runtime in seconds.\n" << std::endl;

    // All time excludes error computation and postprocessing
    std::cout << "NDof, RelativeL2, RelativeEnergy, Cg, Nnz, AllTime, " <<
                 "GridTime, BasisTime, BoundaryTime, AllocationTime, AssemblyTime, " <<
                 "LinearSolveTime, ErrorTime, VtkTime" << std::endl;

    for( size_t refinement = beginStudy; refinement < endStudy; ++refinement )
    {
        mlhp::compute<D>( refinement, degreesString, basisString, writeVtu );
    }
}

} // namespace mlhp

template<size_t...Dimensions> constexpr
auto singularDispatchTable( std::index_sequence<Dimensions...> )
{
    return std::array { ( &mlhp::study<Dimensions + 1> )... };
}

// D, space
int main( int argn, char** argv )
{
    bool writeVtu = false;

    if( argn == 7 && std::string( argv[6] ) == "writeVtu" )
    {
        writeVtu = true;

        argn -= 1;
    }

    if( argn != 6 )
    {
        std::cout << 
            "Invalid number of arguments: " << std::endl <<
            "./singular [1, 2, 3, ...]D, [uniform, graded], [tensor, trunk], beginStudy, endStudy, (optional) writeVtu" << std::endl;
    }
    else
    {
        std::string dim = argv[1];
        constexpr size_t maxDim = 5;

        MLHP_CHECK( dim.size( ) == 2 && dim[1] == 'D', "Invalid dimension string." );

        size_t D = static_cast<size_t>( std::stoi( dim.substr( 0, 1 ) ) );
        MLHP_CHECK( D > 0 && D <= maxDim, "Invalid dimension, maybe not instantiated?" );

        size_t beginStudy = static_cast<size_t>( std::stoi( argv[4] ) );
        size_t endStudy = static_cast<size_t>( std::stoi( argv[5] ) );

        std::cout << "Parameters: " << D << "D, " << argv[2] << ", " << argv[3] 
                  << ", " << beginStudy << ", " << endStudy << std::endl;

        constexpr auto studies = singularDispatchTable( std::make_index_sequence<maxDim>( ) );

        studies[D - 1]( argv[2], argv[3], beginStudy, endStudy, writeVtu );
    }
}
