
#include "mkl.h"

namespace mlhp
{

struct Comparison : public Units
{
    static constexpr size_t D = 3;

    size_t coarsen = 0;
    size_t factor = utilities::binaryPow<size_t>( coarsen );

    double S = 1.0;
    double L = 2.8e5 * J / kg;
    double TlOffset = 200 * C;
    double initialTemperature = 25.0 * C;

    // Laser model
    double laserSpeed = 800.0 * mm / s;
    double laserD4Sigma = 170.0 * um;
    double laserPower = 179.2 * W;
    double laserAbsorptivity = 0.32; 
    double laserDepthScaling = 0.28;

    // Laser source 
    CoordinateList<D> laserPositions
    {
        { -5.0 * mm, 0.0, 0.0 },
        {  2.0 * mm, 0.0, 0.0 }
    };

    laser::LaserTrack<D> laserTrack = laser::makeTrack( laserPositions, laserSpeed, laserPower );
    spatial::ScalarFunction<D + 1> source = laser::volumeSource<D>( laserTrack, laserD4Sigma, laserAbsorptivity, laserDepthScaling );
    double duration = laserTrack.back( ).time;

    // Material
    double density = 8440.0 * kg / ( m * m * m );
 
    NonlinearHeatMaterial material = [=, this]( double T0 ) 
    { 
        double T = std::min( T0, 1290.0 );
        double dT = T0 < 1290.0 ? 1.0 : 0.0;

        return NonlinearHeatParameters
        {
            .specificCapacity      = ( 2.47e-4 * T + 4.05e-1 ) * J / ( g * C ) * density,
            .specificCapacityPrime = ( 2.47e-4 * dT          ) * J / g         * density,
            .conductivity          = ( 1.50e-4 * T + 9.50e-2 ) * W / ( cm * C ),
            .conductivityPrime     = ( 1.50e-4 * dT          ) * W / cm
        }; 
    };

    PhaseChangeParameters phaseChange = 
    {
        .density               = density,
        .latentHeat            = L,
        .liquidConductivity    = 0.0, 
        .Ts                    = 1290.0 * C,
        .Tl                    = 1350.0 * C + TlOffset,
        .S                     = S
    };

    // h- and p-refinement
    PolynomialDegree degree = 3;

    bool useTrunkSpace = true;

    // Refinement based on laser path (delay, sigma, level, zfactor)
    std::vector<laser::Refinement> refinements =
    {
        laser::Refinement { 0.0 * ms, 0.18 * mm, 7.4 - coarsen, 0.5 },
        laser::Refinement { 1.2 * ms, 0.24 * mm, 5.5 - coarsen, 0.5 },
        laser::Refinement { 6.0 * ms, 0.40 * mm, 4.5 - coarsen, 0.8 },
        laser::Refinement { 30.0 * ms, 0.9 * mm, 3.5 - coarsen, 1.0 },
        laser::Refinement { 100.0 * ms, 1.1 * mm, 3.0 - coarsen, 1.0 },
    };

    std::array<double, D> lengths, origin;
    std::array<size_t, D> nelements;
    size_t nslabs = factor;

    Comparison( bool serial, size_t basemesh )
    {
        if( serial )
        {
            parallel::setNumberOfThreads( 1 );
            mkl_set_dynamic(0);
            mkl_set_num_threads(1);
        }

        lengths = { 1.6 * basemesh * cm, 1.6 * basemesh * cm, 1.6 * basemesh * cm };
        nelements = { 2 * basemesh * factor, 2 * basemesh * factor, 2 * basemesh * factor };
        origin = { -lengths[0] / 2.0, -lengths[1] / 2.0, -lengths[2] };
    }

};

} // namespace mlhp

