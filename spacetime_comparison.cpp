// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "helper_comparison.hpp"

namespace mlhp
{

template<size_t D>
auto solveNonlinearProblemComparison( const ElementFilterBasis<D + 1>& basis,
                                      const DofIndicesValuesPair& dirichletDofs, 
                                      const IntegrandFactory<D>& createIntegrand,
                                      std::array<size_t, 2> quadratureOffset = { 1, 0 } )
{
    auto assemble = makeAssembly<D>( basis, dirichletDofs, createIntegrand, quadratureOffset );

    auto df = allocateMatrix<linalg::UnsymmetricSparseMatrix>( basis, dirichletDofs.first );
    auto x = std::vector<double>( df.size1( ), 0.0 );
    auto f = std::vector<double>( df.size1( ), 0.0 );

    double firstNorm = 1.0, norm1 = 1.0;
    size_t iteration = 0;

    for( ; iteration < 14; ++iteration )
    {
//        std::cout << "    residual " << iteration + 1 << ": ";

        // Assemble
        double norm0 = assemble( &df, x, f, iteration );

        firstNorm = iteration != 0 ? firstNorm : norm0;

//        std::cout << norm0 / firstNorm << " (" << norm0 << ")" << std::endl;

        // Solve
        auto dx = pardisoSolve( df, f );

        // Check convergence
        std::transform( x.begin( ), x.end( ), dx.begin( ), x.begin( ), std::minus<double> { } );

        norm1 = assemble( nullptr, x, f, iteration );

        if( norm1 / firstNorm <= 1e-14 && iteration >= 2 )
        {
            break;
        }
    }

//            std::cout << "    final residual: " << norm1 / firstNorm << " (" << norm1 << ")" << std::endl;

            return std::tuple { boundary::inflate( x, dirichletDofs ), iteration };
//    MLHP_THROW( "Nonlinear iterations did not converge." );
}


struct Compute : public Comparison
{
    using Comparison::Comparison;

    auto compute( )
    {
        IntegrandFactory<D> integrandFactory = phaseChangeIntegrandFactory<D>( material, phaseChange, source );

        auto baseGrid = CartesianGrid<D + 1>( array::insert( nelements, D, nslabs ),
                                              array::insert( lengths, D, duration ),
                                              array::insert( origin, D, 0.0 ) );

        auto refinementStrategy = laser::makeRefinement<D>( laserTrack, refinements );

        auto grading = UniformGrading { PolynomialDegreeTuple( { degree, degree, degree, 1 } ) };

        size_t slabSize = 1;

        SlabState<D> slab( baseGrid, slabSize );

        size_t idof = 0, bdof = 0, niterations = 0;

        for( slab.islab = 0; slab.islab < slab.nslabs( ); ++slab.islab )
        {
//            std::cout << "Computing slab " << slab.islab + 1 << " / " << slab.nslabs( );

            // Create one grid including ghost slabs and then filter ghost cells
            auto unfilteredMesh = createWithGhostCells<D>( baseGrid, slab.cell0( ), slab.cell1( ) );

            unfilteredMesh->refine( refinementStrategy );

            auto mesh = filterGhostCells<D>( unfilteredMesh, slab.islab, slab.nslabs( ) );

            // Create basis on unfiltered grid and then remove ghost elements
            auto unfilteredBasis = makeSpaceTimeTrunkSpace<D>( unfilteredMesh, grading );

            auto basis = std::make_shared<ElementFilterBasis<D + 1>>( unfilteredBasis, mesh );

            // Compute initial slab dofs
            if( slab.islab == 0 )
            {
                auto temperatureFunction = spatial::constantFunction<D + 1>( initialTemperature );

                slab.dirichletDofs = boundary::boundaryDofs<D + 1>( temperatureFunction, *basis, { boundary::face( D, 0 ) } );
            }
            else
            {
                auto boundaryFaces = mesh::boundaries( *mesh, { boundary::face( D, 0 ) } )[0];

                slab.dirichletDofs.first = boundary::boundaryDofIndices<D + 1>( *basis, boundaryFaces );
            }

            // Log number of dofs and solve system
            auto nboundary = slab.dirichletDofs.first.size( );
            auto ninternal = basis->ndof( ) - nboundary;

//            std::cout << " (" << ninternal << " internal and " << nboundary << " boundary dofs)" << std::endl;

            auto [dofs, nit] = solveNonlinearProblemComparison<D>( *basis, slab.dirichletDofs, integrandFactory, { 1, 0 } );
            
//            // Postprocess solution
//            std::vector postprocessors = 
//            { 
//                makeSolutionPostprocessor<D + 1>( dofs, 1 ),
//                makeFunctionPostprocessor( source, "Source" ),
//                makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements )
//            };
//            
//            // Postprocess with 6 subcells per element and 4 slices in time per base cell
//            postprocessSlabSlices<D>( array::make<size_t, D>( 3 ), postprocessors, *basis, 
//               "outputs/spacetime_comparison", slab.cell0( ), slab.cell1( ), 4 / factor );
               
            // Extract dofs for next slab
            auto boundaryFaces = mesh::boundaries( *mesh, { boundary::face( D, 1 ) } )[0];

            slab.dirichletDofs.first = boundary::boundaryDofIndices<D + 1>( *basis, boundaryFaces );
            slab.dirichletDofs.second = algorithm::extract( dofs, slab.dirichletDofs.first );

            idof += ninternal;
            bdof += nboundary;
            niterations += nit;

        } // for islab

        return std::tuple { idof, bdof, niterations };

    } // Compute::compute
};

} // namespace mlhp

int main( int argc, char** argv )
{
    MLHP_CHECK( argc == 4, "Usage: spacetime_comparison [serial|parallel] beginStudy endStudy" )

    bool serial = argv[1] == std::string { "serial" };
    size_t beginStudy = std::stoul( argv[2] );
    size_t endStudy = std::stoul( argv[3] );
    
    MLHP_CHECK( serial || argv[1] == std::string { "parallel" }, "First argument must be serial or parallel." );

    std::cout << "Configuration: " << ( serial ? "serial" : "parallel" ) << ", beginStudy: " 
              << beginStudy << ", endStudy: " << endStudy << std::endl;

    for( size_t i = beginStudy; i <= endStudy; ++i )
    {
        auto t0 = mlhp::utilities::tic( );
        auto [idof, bdof, nit] = mlhp::Compute { serial, i }.compute( );
        mlhp::utilities::toc( t0, std::to_string( i ) + ", " + std::to_string( idof ) + ", " + std::to_string( bdof ) + ", " + std::to_string(nit) + ", ", "\n" );
    }
}

