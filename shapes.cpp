#include "mlhp/core/basis.hpp"
#include "mlhp/core/implicit.hpp"
#include "mlhp/core/refinement.hpp"
#include "mlhp/core/postprocessing.hpp"
#include "mlhp/core/basisevaluation.hpp"
#include "mlhp/core/polynomials.hpp"

#include "vtu11/vtu11.hpp"

namespace mlhp
{

void fromHpBasis( )
{
    static constexpr size_t D = 2;

    auto circle = implicit::sphere<D>( array::make<double, D>( 0.0 ), 0.8 );

    auto numberOfBaseElements = array::make<size_t, D>( 1 );
    auto lengths = array::make<double, D>( 1.0 );
    auto polynomialDegrees = array::make<size_t, D>( 1 );
    auto numberOfResultCells = array::add( polynomialDegrees, size_t( 50 ) );

    // Create base grid
    CartesianGrid baseGrid( numberOfBaseElements, lengths );

    auto refinementStrategy = refineTowardsDomainBoundary( circle, 2 );

    // Refine grid
    auto grid = makeRefinedGrid( baseGrid );

    grid->refine( refinementStrategy );

    // Create mlhp basis on refined grid
    auto basis = makeHpBasis<TensorSpace>( grid, polynomialDegrees );

    for( DofIndex iDof = 0; iDof < basis->ndof( ); ++iDof )
    {
        auto postprocessor = makeShapeFunctionPostprocessor<D>( iDof );

        postprocess( numberOfResultCells, { postprocessor }, 
            *basis, "shapes_" + std::to_string( iDof ) + ".vtu" );
    }
}

void manual( bool diffX = false )
{
    std::array<size_t, 2> majorCells { 4, 4 };
    std::array<size_t, 2> minorCells { 50, 50 };

    std::array<size_t, 2> npoints { majorCells[0] * minorCells[0] + 1,
                                    majorCells[1] * minorCells[1] + 1 };

    std::array<size_t, 2> ncells { npoints[0] - 1, npoints[1] - 1 };

    std::array<size_t, 2> degrees { 4, 4 };

    size_t numberOfPoints = array::product( npoints );
    size_t numberOfDofs = ( degrees[0] + 1 ) * ( degrees[1] + 1 );

    // ------------------------------------------------

    std::vector<double> coordinates( 3 * numberOfPoints );
    std::vector<std::vector<double>> values( numberOfDofs, std::vector<double>( numberOfPoints ) );

    std::vector<double> Nr( degrees[0] + 1 );
    std::vector<double> Ns( degrees[1] + 1 );

    // point data
    for( size_t i = 0; i < npoints[0]; ++i )
    {
        for( size_t j = 0; j < npoints[1]; ++j )
        {
            size_t index = i * npoints[1] + j;

            std::array<double, 2> rs { 2.0 * i / ( npoints[0] - 1.0 ) - 1.0,
                                       2.0 * j / ( npoints[1] - 1.0 ) - 1.0 };

            coordinates[3 * index + 0] = rs[0];
            coordinates[3 * index + 1] = rs[1];
            coordinates[3 * index + 2] = 0.0;

            std::vector<double> dummy( degrees[1] + 1, 0.0 );
            std::array<double*, 2> targets{ diffX ? dummy.data( ) : Nr.data( ),
                                            diffX ? Nr.data( ) : dummy.data( ) };

            polynomial::integratedLegendre<1>( degrees[0], rs[0], targets );
            polynomial::integratedLegendre<0>( degrees[1], rs[1], { Ns.data( ) } );

            for( size_t k = 0; k < degrees[0] + 1; ++k )
            {
                for( size_t l = 0; l < degrees[1] + 1; ++l )
                {
                    values[k * ( degrees[1] + 1 ) + l][index] = Nr[k] * Ns[l];
                }
            }
        }
    }

    std::vector<vtu11::VtkIndexType> connectivities1, connectivities2;
    std::vector<vtu11::VtkIndexType> offsets1, offsets2;
    std::vector<vtu11::VtkCellType> types1, types2;

    // quads
    for( size_t i = 0; i < ncells[0]; ++i )
    {
        for( size_t j = 0; j < ncells[1]; ++j )
        {
            connectivities1.push_back( static_cast<vtu11::VtkIndexType>( i * npoints[1] + j ) );
            connectivities1.push_back( static_cast<vtu11::VtkIndexType>( i * npoints[1] + j + 1 ) );
            connectivities1.push_back( static_cast<vtu11::VtkIndexType>( ( i + 1 ) * npoints[1] + j + 1 ) );
            connectivities1.push_back( static_cast<vtu11::VtkIndexType>( ( i + 1 ) * npoints[1] + j ) );

            offsets1.push_back( static_cast<vtu11::VtkIndexType>( 4 * ( i * ncells[1] + j + 1 ) ) );
            types1.push_back( 9 );
        }
    }

    size_t offset = 2;

    // x-lines
    for( size_t i = 0; i < majorCells[1] + 1; ++i )
    {
        for( size_t j = 0; j < ncells[0]; ++j )
        {
            size_t index = j * npoints[1] + i * minorCells[1];

            connectivities2.push_back( static_cast<vtu11::VtkIndexType>( index ) );
            connectivities2.push_back( static_cast<vtu11::VtkIndexType>( index + npoints[1] ) );

            offsets2.push_back( static_cast<vtu11::VtkIndexType>( offset ) );

            types2.push_back( 3 );

            offset += 2;
        }
    }

    // y-lines
    for( size_t i = 0; i < majorCells[0] + 1; ++i )
    {
        for( size_t j = 0; j < ncells[1]; ++j )
        {
            size_t index = i * minorCells[0] * npoints[1] + j;

            connectivities2.push_back( static_cast<vtu11::VtkIndexType>( index ) );
            connectivities2.push_back( static_cast<vtu11::VtkIndexType>( index + 1 ) );

            offsets2.push_back( static_cast<vtu11::VtkIndexType>( offset ) );

            types2.push_back( 3 );

            offset += 2;
        }
    }

    // write files
    vtu11::Vtu11UnstructuredMesh mesh1 { coordinates, connectivities1, offsets1, types1 };
    vtu11::Vtu11UnstructuredMesh mesh2 { coordinates, connectivities2, offsets2, types2 };

    for( size_t i = 0; i < numberOfDofs; ++i )
    {
        vtu11::DataSetInfo metadata { "Shapes", vtu11::DataSetType::PointData, 1 };

        auto name1 = "shapes_fills_" + std::to_string( i ) + ".vtu";
        auto name2 = "shapes_lines_" + std::to_string( i ) + ".vtu";

        vtu11::writeVtu( name1, mesh1, { metadata }, { values[i] } );
        vtu11::writeVtu( name2, mesh2, { metadata }, { values[i] } );
    }
}

// Print basis functions in r and s in 2D space
void manual1D( size_t axis )
{
    size_t p = 4;
    size_t ndofs = p + 1;

    std::vector<vtu11::VtkIndexType> connectivities, offsets;
    std::vector<vtu11::VtkCellType> types;
    std::vector<double> coordinates;
    std::vector<std::vector<double>> values( ndofs );

    size_t ncells = 100;

    std::vector<double> N( ndofs );

    for( size_t i = 0; i < ncells + 1; ++i )
    {
        double r = ( 2.0 * i ) / ncells - 1.0;

        polynomial::integratedLegendre<0>( p, r, { N.data( ) } );

        if( axis == 0 )
        {
            coordinates.push_back( r );
            coordinates.push_back( 0.0 );
            coordinates.push_back( 0.0 );
        }
        else
        {
            coordinates.push_back( 0.0 );
            coordinates.push_back( r );
            coordinates.push_back( 0.0 );
        }

        for( size_t j = 0; j < ndofs; ++j )
        {
            values[j].push_back( N[j] );
        }
    }

    for( size_t i = 0; i < ncells; ++i )
    {
        connectivities.push_back( static_cast<vtu11::VtkIndexType>( i ) );
        connectivities.push_back( static_cast<vtu11::VtkIndexType>( i + 1 ) );
        offsets.push_back( static_cast<vtu11::VtkIndexType>( 2 * ( i + 1 ) ) );
        types.push_back( 3 );
    }

    vtu11::Vtu11UnstructuredMesh mesh { coordinates, connectivities, offsets, types };

    for( size_t i = 0; i < ndofs; ++i )
    {
        vtu11::DataSetInfo metadata { "Shapes", vtu11::DataSetType::PointData, 1 };

        auto name = "shapes1D_" + std::to_string( axis ) + "_" + std::to_string( i ) + ".vtu";

        vtu11::writeVtu( name, mesh, { metadata }, { values[i] } );
    }
}

} // mlhp

int main( )
{
//    mlhp::manual1D( 0 );
//    mlhp::manual1D( 1 );

    mlhp::manual( false );

    //mlhp::fromHpBasis( );
}
