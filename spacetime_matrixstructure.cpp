// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "helper_comparison.hpp"

namespace mlhp
{

template<size_t D>
auto makeIntegrand( const spatial::ScalarFunction<D + 1>& capacity,
                    const spatial::ScalarFunction<D + 1>& conductivity,
                    const spatial::ScalarFunction<D + 1>& source,
                    const AbsBasis<D + 1>& basis,
                    bool petrov, bool filter )
{

    auto evaluate = [=, &basis]( const BasisFunctionEvaluation<D + 1>& shapes,
                                 AlignedDoubleVectors& targets, double weightDetJ )
    { 
        auto ndof = shapes.ndof( );
        auto nblocks = shapes.nblocks( );
        auto ndofpadded = shapes.ndofpadded( );

        double c = capacity( shapes.xyz( ) );
        double k = conductivity( shapes.xyz( ) );
        double f = source( shapes.xyz( ) );

        auto& testfilter = createTestFilter<D>( basis, shapes, filter );

        auto trialN = shapes.noalias( 0, 0 );
        auto trialDN = shapes.noalias( 0, 1 );

        auto testN = testFunctions<D>( shapes, petrov );
        auto testDN = testGradient<D>( shapes, petrov );

        linalg::elementRhs( targets[1].data( ), ndof, nblocks, [&](size_t i)
        { 
            return testN[i] * f * weightDetJ * testfilter[i];
        } );     

        linalg::unsymmetricElementLhs( targets[0].data( ), ndof, nblocks, [=]( size_t i, size_t j )
        { 
            double value = c * testN[i] * trialDN[D * ndofpadded + j];

            for( size_t axis = 0; axis < D; ++axis )
            {
                value += k * testDN[axis][i] * trialDN[axis * ndofpadded + j];
            }

            return value * weightDetJ * testfilter[i];
        } );
    };

    auto diffOrder = petrov ? DiffOrders::SecondDerivatives : DiffOrders::FirstDerivatives;
    auto types = std::vector { AssemblyType::UnsymmetricMatrix, AssemblyType::Vector };

    return DomainIntegrand<D + 1>( types, diffOrder, evaluate );
}

auto doMain( bool petrov, bool filter, double factor )
{
    static constexpr size_t D = 1;

    auto p = size_t { 1 };
    auto resolution = array::make<size_t, D + 1>( 7 );

    auto capacity = spatial::constantFunction<D + 1>( factor );
    auto conductivity = spatial::constantFunction<D + 1>( factor );
    
    std::function source = spatial::constantFunction<D + 1>( factor );

    auto scaleX = 1.0 / 3.0;
    auto scaleT = 1.0 / 3.0;

    auto ticks = std::array
    {
        std::vector { 0.0, 1 * scaleX, 2 * scaleX, 3 * scaleX },
        std::vector { 0.0, 1 * scaleT, 2 * scaleT, 3 * scaleT }
    };

    // Make and refine grid
    auto grid = makeRefinedGrid( ticks );

    // Create multi-level hp basis on refined grid
    auto basis_ = makeHpBasis<TensorSpace>( grid, p );

    auto filteredMesh = std::make_shared<FilteredMesh<D + 1>>( grid, CellIndexVector { } );
    auto basis = std::make_shared<ElementFilterBasis<D + 1>>( basis_, filteredMesh );

    // Allocate sparse matrix
    auto boundaryDofs = boundary::boundaryDofs<D + 1>( spatial::constantFunction<D + 1>( 0.0 ), 
        *basis, { boundary::face( 0, 0 ), boundary::face( 1, 0 ) } );

    auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, boundaryDofs.first );
    auto vector = std::vector<double>( matrix.size1( ), 0.0 );

    // Assemble global system
    auto integrand = makeIntegrand<D>( capacity, conductivity, source, *basis, petrov, filter );
    
    auto quadrature = makeIntegrationOrderDeterminor<D + 1>( 10 );

    integrateOnDomain( *basis, integrand, { matrix, vector }, quadrature, boundaryDofs );

    auto map = std::vector<size_t>(matrix.size2( ), 0);
    
    for( size_t i = 0; i < ticks[1].size( ) - 1; ++i )
    {
        for( size_t j = 0; j < ticks[0].size( ) - 1; ++j )
        {
            map[i * ( ticks[1].size( ) - 1 ) + j] = j * ( ticks[1].size( ) - 1 ) + i;
        }
    }

    auto width = 12;
    auto pres = 8;

    std::cout << "       ";
    for( size_t j = 0; j < matrix.size2( ); ++j )
    {
        std::cout << std::setw(width) << j;
    }

    std::cout << std::endl << "       " << std::string(width * matrix.size2( ), '_') << std::endl;
 
    for( size_t i = 0; i < matrix.size1( ); ++i )
    { 
        std::cout << std::setw(4) << i << "  |";
 
        for( size_t j = 0; j < matrix.size2( ); ++j )
        {
            std::cout << std::setw(width) << std::setprecision(pres) << matrix(map[i], map[j]);
        }

        std::cout << "    = " << std::setw(width) << std::setprecision(pres) << vector[map[i]] << std::endl;
    }


    // Solve global system
    auto interiorSolution = pardisoSolve( matrix, vector );
    auto fullSolution = boundary::inflate( interiorSolution, boundaryDofs );

    std::cout << std::endl << "u = [";
    for( size_t j = 0; j < interiorSolution.size( ); ++j )
    {
        std::cout << std::setw(width) << std::setprecision(pres) << interiorSolution[map[j]];
    }
    std::cout << "]" << std::endl;

    auto postprocessNumericalSolution = makeSolutionPostprocessor<D + 1>( fullSolution, 1 );
    auto postprocessSource = makeFunctionPostprocessor<D + 1>( source, "Source" );
  
    postprocess( resolution, { postprocessNumericalSolution, postprocessSource },
                 *basis, "outputs/spacetime_matrixstructure" );

    return 0;
}

} // namespace mlhp

int main( int argn, char** argv )
{
    auto factor = 1.0;

    if( argn == 3 )
    {
        try
        {
            factor = std::stod( argv[2] );
        }
        catch( const std::invalid_argument& ex )
        {
            std::cout << "Conversion error." << std::endl;
    
            argn = 1;
        }

        argn -= 1;
    }

    if( argn == 2 )
    {
        std::string arg = argv[1];

        if( arg == "same" )
        {
            return mlhp::doMain( false, false, factor );
        }

        if( arg == "constant" )
        {
            return mlhp::doMain( true, true, factor );
        }

        if( arg == "derivative" )
        {
            return mlhp::doMain( true, false, factor );
        }
    }

    std::cout << "Usage: ./spacetime_matrixstructure <same/constant/derivative> [scaling factor]\nExample: ./spacetime_matrixstructure same 36" << std::endl;
}
