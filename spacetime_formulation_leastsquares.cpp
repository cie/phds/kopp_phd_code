// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "helper_mkl.hpp"
//#include "mkl.h"

namespace mlhp
{

template<size_t D>
auto prepareTmp( const BasisFunctionEvaluation<D + 1>& shapes,
                 memory::AlignedVector<double>& tmp,
                 double capacity, double conductivity )
{
    auto ndofpadded = shapes.ndofpadded( );

    tmp.resize( ( D + 1 ) * ndofpadded );

    std::fill( tmp.begin( ), tmp.end( ), 0.0 );

    auto write = [&]( size_t row, size_t field, size_t diff, size_t axis, double factor )
    {
        auto target = tmp.data( ) + row * ndofpadded;

        for( size_t ifield = 0; ifield < field; ++ifield )
        {
            target += shapes.ndof( ifield );
        }

        auto ndof = shapes.ndof( field );
        auto N = shapes.noalias( field, diff ) + axis * shapes.ndofpadded( field );

        for( size_t k = 0; k < ndof; ++k )
        {
            target[k] = N[k] * factor;
        }
    };

    double beta = 800.0;

    // Upper left block
    write( 0, 0, 1, D, capacity );

    // Upper right blocks
    for( size_t jblock = 1; jblock <= D; ++jblock )
    {
        write( 0, jblock, 1, jblock - 1, -1.0 );
    }

    // Lower left blocks
    for( size_t iblock = 1; iblock <= D; ++iblock )
    {
        write( iblock, 0, 1, iblock - 1, -beta * conductivity );
    }

    // Diagonal lower right blocks
    for( size_t dblock = 1; dblock <= D; ++dblock )
    {
        write( dblock, dblock, 0, 0, beta );
    }

    return memory::assumeAlignedNoalias( tmp.data( ) );
}

template<size_t D>
auto linearLeastSquaresSpaceTimeIntegrand1( double capacity, double conductivity,
                                            const spatial::ScalarFunction<D + 1>& source )
{
    auto evaluate = [=]( const BasisFunctionEvaluation<D + 1>& shapes,
                         const LocationMap&,
                         AlignedDoubleVectors& targets,
                         AlignedDoubleVector& tmp,
                         double weightDetJ )
    { 
        double f = source( shapes.xyz( ) );

        auto B = prepareTmp<D>( shapes, tmp, capacity, conductivity );

        auto ndof = shapes.ndof( );
        auto padded = shapes.ndofpadded( );
        auto nblocks = shapes.nblocks( );

        linalg::symmetricElementLhs( targets[0].data( ), ndof, nblocks, [&]( size_t i, size_t j )
        {
            double value = 0.0;

            for( size_t k = 0; k < D + 1; ++k )
            {
                value += B[k * padded + i] * B[k * padded + j];
            }

            return value * weightDetJ;
        } );

        linalg::elementRhs( targets[1].data( ), ndof, nblocks, [&]( size_t i )
        {
            return B[i] * f * weightDetJ;
        } );
    };

    auto types = std::vector { AssemblyType::SymmetricMatrix, AssemblyType::Vector };

    return DomainIntegrand<D + 1>( DiffOrders::FirstDerivatives, types, evaluate );
}

// --------------------

template<size_t D>
auto extractSizes( const BasisFunctionEvaluation<D + 1>& shapes )
{
    std::array<size_t, D + 1> ndofs { };
    std::array<size_t, D + 1> padded { };
    std::array<size_t, D + 2> offsets { };

    for( size_t ifield = 0; ifield < D + 1; ++ifield )
    {
        ndofs[ifield] = shapes.ndof( ifield );
        padded[ifield] = shapes.ndofpadded( ifield );
        offsets[ifield + 1] = offsets[ifield] + ndofs[ifield];
    }

    auto allpadded = memory::paddedLength<double>( offsets.back( ) );

    return std::make_tuple( ndofs, padded, offsets, allpadded );
}

template<size_t D>
auto linearLeastSquaresSpaceTimeIntegrand2( double capacity, double conductivity,
                                            const spatial::ScalarFunction<D + 1>& source )
{
    auto evaluate = [=]( const BasisFunctionEvaluation<D + 1>& shapes,
                         AlignedDoubleVectors& targets, double weightDetJ )
    { 
        double f = source( shapes.xyz( ) );

        auto lhs = memory::assumeAlignedNoalias( targets[0].data( ) );
        auto rhs = memory::assumeAlignedNoalias( targets[1].data( ) );

        auto [ndofs, padded, offsets, allpadded] = extractSizes<D>( shapes );

        double beta = std::pow( 800.0, 2 );

        // Upper left
        {
            auto dN = shapes.noalias( 0, 1 );

            for( size_t idof = 0; idof < ndofs[0]; ++idof )
            {
                for( size_t jdof = 0; jdof < ndofs[0]; ++jdof )
                {
                    double value = capacity * dN[D * padded[0] + idof] * capacity * dN[D * padded[0] + jdof];

                    for( size_t axis = 0; axis < D; ++axis )
                    {
                        value += beta * conductivity * dN[axis * padded[0] + idof] * conductivity * dN[axis * padded[0] + jdof];
                    } // axis

                    lhs[idof * allpadded + jdof] += value * weightDetJ;

                } // jdof
            } // idof
        } 

        // Upper right row
        for( size_t jblock = 0; jblock < D; ++jblock )
        {
            auto [Ni, dNi] = shapes.template noalias<1>( 0 );
            auto [Nj, dNj] = shapes.template noalias<1>( jblock + 1 );

            for( size_t idof = 0; idof < ndofs[0]; ++idof )
            {
                for( size_t jdof = 0; jdof < ndofs[jblock + 1]; ++jdof )
                {
                    double value = - capacity * dNi[D * padded[0] + idof] * dNj[jblock * padded[jblock + 1] + jdof]
                                   - conductivity * beta * dNi[jblock * padded[0] + idof] * Nj[jdof];

                    lhs[idof * allpadded + offsets[jblock + 1] + jdof] += value * weightDetJ;

                } // jdof
            } // idof
        }

        // Lower left column
        for( size_t iblock = 0; iblock < D; ++iblock )
        {
            auto [Ni, dNi] = shapes.template noalias<1>( iblock + 1 );
            auto [Nj, dNj] = shapes.template noalias<1>( 0 );

            for( size_t idof = 0; idof < ndofs[iblock + 1]; ++idof )
            {
                for( size_t jdof = 0; jdof < ndofs[0]; ++jdof )
                {
                    double value = - dNi[iblock * padded[iblock + 1] + idof] * capacity * dNj[D * padded[0] + jdof] 
                                   - Ni[idof] * beta * conductivity * dNj[iblock * padded[0] + jdof];

                    lhs[(idof + offsets[iblock + 1]) * allpadded + jdof] += value * weightDetJ;

                } // jdof
            } // idof
        }

        // Lower right block
        for( size_t iblock = 0; iblock < D; ++iblock )
        {
            for( size_t jblock = 0; jblock < D; ++jblock )
            {
                auto [Ni, dNi] = shapes.template noalias<1>( iblock + 1 );
                auto [Nj, dNj] = shapes.template noalias<1>( jblock + 1 );

                for( size_t idof = 0; idof < ndofs[iblock + 1]; ++idof )
                {
                    double factor = iblock == jblock ? beta * Ni[idof] : 0.0; /* 0.0; Ni[idof] * 100000000.0; iblock == jblock ? Ni[idof] : 0.0;*/

                    for( size_t jdof = 0; jdof < ndofs[jblock + 1]; ++jdof )
                    {
                        double value = factor * Nj[jdof] + dNi[iblock * padded[iblock + 1] + idof] * 
                                                           dNj[jblock * padded[jblock + 1] + jdof];

                        lhs[( idof + offsets[iblock + 1] ) * allpadded + offsets[jblock + 1] + jdof] += value * weightDetJ;

                    } // jdof
                } // idof
            } 
        }

        // Right hand side
        {
            auto dN0 = shapes.noalias( 0, 1 );

            for( size_t idof = 0; idof < ndofs[0]; ++idof )
            {
                rhs[idof] += capacity * dN0[D * padded[0] + idof] * f * weightDetJ;
            }

            for( size_t iblock = 0; iblock < D; ++iblock )
            {
                auto dNi = shapes.noalias( iblock + 1, 1 );

                for( size_t idof = 0; idof < ndofs[iblock + 1]; ++idof )
                {
                    rhs[offsets[iblock + 1] + idof] += -dNi[iblock * padded[iblock + 1] + idof] * f * weightDetJ;
                }
            }
        }
    };

    auto types = std::vector { AssemblyType::UnsymmetricMatrix, AssemblyType::Vector };

    return DomainIntegrand<D + 1>( types, DiffOrders::FirstDerivatives, evaluate );
}


struct Compute : public Units
{
    void compute( )
    {
        static constexpr size_t D = 3;

        // Domain, base mesh and initial condition
        //auto lengths = std::array<double, D> { 2 * mm, 2 * mm, 0.3 * mm };
        //auto nelements = std::array<size_t, D> { 4, 4, 1 };

        //auto lengths = std::array<double, D> { 1 * mm, 0.5 * mm, 0.06 * mm };
        //auto nelements = std::array<size_t, D> { 12, 6, 1 };
        //auto nslabs = size_t { 15 };

        auto lengths = std::array<double, D> { 24.08 * mm, 24.82 * mm, 3.18 * mm };
        auto nelements = std::array<size_t, D> { 12, 12, 3 };
        auto nslabs = size_t { 1 };

        // Laser source 
        CoordinateList<D> laserPositions
        {
            //{ 1.0 * lengths[0] / 6.0, 1.0 * lengths[1] / 6.0, lengths[2] },
            //{ 5.0 * lengths[0] / 6.0, 5.0 * lengths[1] / 6.0, lengths[2] },
            {  lengths[0] / 2.0 - 1.4 * cm / 16.0,  lengths[1] / 2.0, lengths[2] },
            {  lengths[0] / 2.0 + 1.4 * cm / 16.0,  lengths[1] / 2.0, lengths[2] },
        };

        auto laserTrack = laser::makeTrack( laserPositions, 0.8 * m / s, 180.0 * W );
        //laserTrack.front( ).intensity = 0.0;
        auto source = laser::volumeSource<D>( laserTrack, 170 * um, 0.32, 0.28 );
        auto duration = laserTrack.back( ).time;

        //auto slabDuration = 0.6 * ms;
        //auto nslabs = static_cast<size_t>( std::ceil( duration / slabDuration ) );


        // Discretization
        std::vector refinements =
        {
            //laser::Refinement { 0.0 * ms, 0.18 * mm, 3.4, 0.5 },
            //laser::Refinement { 1.2 * ms, 0.24 * mm, 1.5, 0.5 },
            //laser::Refinement { 6.0 * ms, 0.40 * mm, 0.5, 0.8 },
            //laser::Refinement { 30.0 * ms, 0.9 * mm, 1.5, 1.0 },
            laser::Refinement { 0.0 * ms, 0.18 * mm, 5.4, 0.5 },
            laser::Refinement { 1.2 * ms, 0.24 * mm, 3.5, 0.5 },
            laser::Refinement { 6.0 * ms, 0.40 * mm, 2.5, 0.8 },
            laser::Refinement { 30.0 * ms, 0.9 * mm, 1.5, 1.0 },

        };

        auto refinementStrategy = laser::makeRefinement<D>( laserTrack, refinements );

        auto mesh = makeRefinedGrid( array::insert( nelements, D, size_t { nslabs } ),
                                     array::insert( lengths, D, duration ) );
    
        mesh->refine( refinementStrategy );

        auto degree = PolynomialDegree { 3 };

        auto grading = std::function { [=]( const AbsHierarchicalGrid<D + 1>& grid, size_t nfields )
        {
            AnsatzTemplateVector ansatzTemplates( { grid.ncells( ), nfields, D + 1 }, 0 );

            for( size_t ielement = 0; ielement < ansatzTemplates.shape( )[0]; ++ielement )
            {
                for( size_t axis = 0; axis < D; ++axis )
                {
                    ansatzTemplates( ielement, 0, axis ) = degree;

                    for( size_t ifield = 0; ifield < D; ++ifield )
                    {
                        ansatzTemplates( ielement, ifield + 1, axis ) = std::max( degree, PolynomialDegree { 2 } ) - 1;
                    }
                }

                for( size_t ifield = 0; ifield < D + 1; ++ifield )
                {
                    ansatzTemplates( ielement, ifield, D ) = 1;
                }
            }

            return ansatzTemplates;
        } };

        auto mlhpBasis = makeHpBasis<TrunkSpace>( mesh, grading, D + 1 );

        auto filteredMesh = std::make_shared<FilteredMesh<D + 1>>(mesh, CellIndexVector { });
        auto basis = std::make_shared<ElementFilterBasis<D + 1>>(mlhpBasis, filteredMesh);

        std::cout << "Least-squares formulation:" << std::endl;
        std::cout << "duration :" << duration * 1000 << std::endl;
        std::cout << "nslabs: " << nslabs << std::endl;
        std::cout << "ndof: " << basis->ndof( ) << std::endl;

        // Assembly
        auto t0 = utilities::tic( "Assembly ... " );

        double density = 8500.0 * kg / ( m * m * m );
        double specificCapacity = 0.5 * J / ( g * C );
        double conductivity = 0.17 * W / ( cm * C );

        auto integrand = linearLeastSquaresSpaceTimeIntegrand1<D>(
            specificCapacity * density, conductivity, source );
        //auto integrand = linearLeastSquaresSpaceTimeIntegrand1<D>( 1.0, 1.0, source );

        auto initialTemperature = spatial::constantFunction<D + 1>( 25.0 );

        //auto dirichletDofs0 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 0 );
        //auto dirichletDofs1 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 1 );
        //auto dirichletDofs2 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 2 );
        //auto dirichletDofs3 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 3 );

        std::vector<DofIndicesValuesPair> dirichletFaceDofs =
        {
            boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 0 ),
            //boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::allFaces( D ) }, 0 )
        };

        for( size_t axis = 0; axis < D; ++axis )
        {
            dirichletFaceDofs.push_back( boundary::boundaryDofs<D + 1>( spatial::constantFunction<D + 1>( 0.0 ), *basis,
                { boundary::face( D, 0 ), boundary::face( axis, 0 ), boundary::face( axis, 1 ) }, axis + 1 ) );
        }

        auto dirichletDofs = boundary::combine( dirichletFaceDofs );

        auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, dirichletDofs.first );
        auto rhs = std::vector<double>( matrix.size1( ) );

        integrateOnDomain( *basis, integrand, { matrix, rhs }, dirichletDofs );

        utilities::toc( t0, "Took: " );

        // Linear solution
        auto t1 = utilities::tic( "Solving ... " );
        //auto solution = boundary::inflate( pardisoSolve( matrix, rhs ), dirichletDofs );
        auto solution = boundary::inflate( makeMklCGSolver( )( matrix, rhs ), dirichletDofs );
        //auto solution = boundary::inflate( linalg::makeCGSolver( )( matrix, rhs ), dirichletDofs );
        utilities::toc( t1, "Took: " );

        // Postprocessing
        auto nsamples = array::add<size_t>( array::make<size_t, D>( degree ), 4 );

        std::vector postprocessors =
        {
            makeSolutionPostprocessor<D + 1>( solution, D + 1 ),
            makeFunctionPostprocessor<D + 1>( source, "Source" ),
        };

        postprocessSlabSlices<D>( nsamples, postprocessors, *basis, "spacetime_formulation_leastsquares", 0, nslabs, 4 );

    } // Compute::compute
};

} // namespace mlhp

int main( int argc, char** argv )
{
    mlhp::Compute { }.compute( );
}

