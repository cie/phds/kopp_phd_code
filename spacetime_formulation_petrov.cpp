// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "mkl.h"

namespace mlhp
{

template<size_t D>
auto linearPetrovGalerkinSpaceTimeIntegrand( double capacity, double conductivity,
                                             const spatial::ScalarFunction<D + 1>& source )
{
    auto evaluate = [=]( const BasisFunctionEvaluation<D + 1>& shapes,
                         AlignedDoubleVectors& targets, double weightDetJ )
    { 
        auto ndof = shapes.ndof( );
        auto nblocks = shapes.nblocks( );
        auto ndofpadded = shapes.ndofpadded( );

        double f = source( shapes.xyz( ) );

        auto trialN = shapes.noalias( 0, 0 );
        auto trialDN = shapes.noalias( 0, 1 );

        auto testN = testFunctions<D>( shapes, true );
        auto testDN = testGradient<D>( shapes, true );

        linalg::elementRhs( targets[1].data( ), ndof, nblocks, [&]( size_t i )
        { 
            return testN[i] * f * weightDetJ;
        } );     

        linalg::unsymmetricElementLhs( targets[0].data( ), ndof, nblocks, [=]( size_t i, size_t j )
        { 
            double value = testN[i] * capacity * trialDN[D * ndofpadded + j];

            for( size_t axis = 0; axis < D; ++axis )
            {
                value += testDN[axis][i] * conductivity * trialDN[axis * ndofpadded + j];

            } // component

            return value * weightDetJ;
        } );
    };

    auto types = std::vector { AssemblyType::UnsymmetricMatrix, AssemblyType::Vector };

    return DomainIntegrand<D + 1>( types, DiffOrders::SecondDerivatives, evaluate );
}

struct Compute : public Units
{
    void compute( )
    {
        static constexpr size_t D = 3;

        // Domain, base mesh and initial condition
        //auto lengths = std::array<double, D> { 2 * mm, 2 * mm, 0.3 * mm };
        //auto nelements = std::array<size_t, D> { 4, 4, 1 };

        //auto lengths = std::array<double, D> { 1 * mm, 0.5 * mm, 0.06 * mm };
        //auto nelements = std::array<size_t, D> { 12, 6, 1 };
        //auto nslabs = size_t { 15 };

        auto lengths = std::array<double, D> { 24.08 * mm, 24.82 * mm, 3.18 * mm };
        auto nelements = std::array<size_t, D> { 12, 12, 3 };
        auto nslabs = size_t { 1 };

        // Laser source 
        CoordinateList<D> laserPositions
        {
            //{ 1.0 * lengths[0] / 6.0, 1.0 * lengths[1] / 6.0, lengths[2] },
            //{ 5.0 * lengths[0] / 6.0, 5.0 * lengths[1] / 6.0, lengths[2] }
            {  lengths[0] / 2.0 - 1.4 * cm / 16.0,  lengths[1] / 2.0, lengths[2] },
            {  lengths[0] / 2.0 + 1.4 * cm / 16.0,  lengths[1] / 2.0, lengths[2] },
        };

        auto laserTrack = laser::makeTrack( laserPositions, 0.8 * m / s, 180.0 * W );
        //laserTrack.front( ).intensity = 0.0;
        auto source = laser::volumeSource<D>( laserTrack, 170 * um, 0.32, 0.28 );
        auto duration = laserTrack.back( ).time;

        //auto slabDuration = 0.6 * ms;
        //auto nslabs = static_cast<size_t>( std::ceil( duration / slabDuration ) );

        size_t polynomialDegree = 3;

        // Discretization
        std::vector refinements =
        {
            //laser::Refinement { 0.0 * ms, 0.18 * mm, 3.4, 0.5 },
            //laser::Refinement { 1.2 * ms, 0.24 * mm, 1.5, 0.5 },
            //laser::Refinement { 6.0 * ms, 0.40 * mm, 0.5, 0.8 },
            //laser::Refinement { 30.0 * ms, 0.9 * mm, 1.5, 1.0 },
            laser::Refinement { 0.0 * ms, 0.18 * mm, 5.4, 0.5 },
            laser::Refinement { 1.2 * ms, 0.24 * mm, 3.5, 0.5 },
            laser::Refinement { 6.0 * ms, 0.40 * mm, 2.5, 0.8 },
            laser::Refinement { 30.0 * ms, 0.9 * mm, 1.5, 1.0 },
        };

        auto refinementStrategy = laser::makeRefinement<D>( laserTrack, refinements );

        auto mesh = makeRefinedGrid( array::insert( nelements, D, size_t { nslabs } ),
                                     array::insert( lengths, D, duration ) );
    
        mesh->refine( refinementStrategy );

        auto degrees = array::insert( array::make<size_t, D>( polynomialDegree ), D, size_t { 1 } );

        auto mlhpBasis = makeHpBasis<TrunkSpace>( mesh, UniformGrading { degrees } );

        auto filteredMesh = std::make_shared<FilteredMesh<D + 1>>(mesh, CellIndexVector { });
        auto basis = std::make_shared<ElementFilterBasis<D + 1>>(mlhpBasis, filteredMesh);

        std::cout << "Petrov-Galerkin formulation:" << std::endl;
        std::cout << "duration :" << duration * 1000 << std::endl;
        std::cout << "nslabs: " << nslabs << std::endl;
        std::cout << "ndof: " << basis->ndof( ) << std::endl;

        // Assembly
        auto t0 = utilities::tic( "Assembly ... " );

        double density = 8500.0 * kg / ( m * m * m );
        double specificCapacity = 0.5 * J / ( g * C );
        double conductivity = 0.17 * W / ( cm * C );

        auto integrand = linearPetrovGalerkinSpaceTimeIntegrand<D>(
            specificCapacity * density, conductivity, source );

        auto initialTemperature = spatial::constantFunction<D + 1>( 25.0 );

        auto dirichletDofs = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) } );

        auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, dirichletDofs.first );
        auto rhs = std::vector<double>( matrix.size1( ) );

        integrateOnDomain( *basis, integrand, { matrix, rhs }, dirichletDofs );

        utilities::toc( t0, "Took: " );

        // Linear solution
        auto t1 = utilities::tic( "Solving ... " );

        auto solution = boundary::inflate( pardisoSolve( matrix, rhs ), dirichletDofs );

        utilities::toc( t1, "Took: " );

        // Postprocessing
        auto nsamples = array::add<size_t>( array::slice( degrees, D ), 4 );

        std::vector postprocessors =
        {
            makeSolutionPostprocessor<D + 1>( solution, 1 ),
            makeFunctionPostprocessor<D + 1>( source, "Source" ),
        };

        postprocessSlabSlices<D>( nsamples, postprocessors, *basis, "spacetime_formulation_petrov", 0, nslabs, 4 );

    } // Compute::compute
};

} // namespace mlhp

int main( int argc, char** argv )
{
    mlhp::Compute { }.compute( );
}

