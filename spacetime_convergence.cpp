// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "helper_comparison.hpp"

namespace mlhp
{

template<size_t D>
auto makeIntegrand( const spatial::ScalarFunction<D + 1>& capacity,
                    const spatial::ScalarFunction<D + 1>& conductivity,
                    const spatial::ScalarFunction<D + 1>& source,
                    const AbsBasis<D + 1>& basis,
                    bool petrov = true )
{

    auto evaluate = [=, &basis]( const BasisFunctionEvaluation<D + 1>& shapes,
                                 AlignedDoubleVectors& targets, double weightDetJ )
    { 
        auto ndof = shapes.ndof( );
        auto nblocks = shapes.nblocks( );
        auto ndofpadded = shapes.ndofpadded( );

        double c = capacity( shapes.xyz( ) );
        double k = conductivity( shapes.xyz( ) );
        double f = source( shapes.xyz( ) );

        auto& testfilter = createTestFilter<D>( basis, shapes, petrov );      

        auto trialN = shapes.noalias( 0, 0 );
        auto trialDN = shapes.noalias( 0, 1 );

        auto testN = testFunctions<D>( shapes, petrov );
        auto testDN = testGradient<D>( shapes, petrov );

        linalg::elementRhs( targets[1].data( ), ndof, nblocks, [&](size_t i)
        { 
            return testN[i] * f * weightDetJ * testfilter[i];
        } );     

        linalg::unsymmetricElementLhs( targets[0].data( ), ndof, nblocks, [=]( size_t i, size_t j )
        { 
            double value = c * testN[i] * trialDN[D * ndofpadded + j];

            for( size_t axis = 0; axis < D; ++axis )
            {
                value += k * testDN[axis][i] * trialDN[axis * ndofpadded + j];
            }

            return value * weightDetJ * testfilter[i];
        } );
    };

    auto diffOrder = petrov ? DiffOrders::SecondDerivatives : DiffOrders::FirstDerivatives;
    auto types = std::vector { AssemblyType::UnsymmetricMatrix, AssemblyType::Vector };

    return DomainIntegrand<D + 1>( types, diffOrder, evaluate );
}

DomainIntegrand<2> makeL2H1ErrorIntegrand( const std::vector<double>& solutionDofs,
                                           const spatial::ScalarFunction<2>& solution,
                                           const spatial::ScalarFunction<2>& spatialDerivative )
{
    auto evaluate = [=, &solutionDofs]( const BasisFunctionEvaluation<2>& shapes,
                                        const LocationMap& locationMap,
                                        AlignedDoubleVectors& targets,
                                        AlignedDoubleVector&, 
                                        double weightDetJ )
    {
        auto uA = solution( shapes.xyz( ) );
        auto duA = spatialDerivative( shapes.xyz( ) );

        auto uN = evaluateSolution( shapes, locationMap, solutionDofs );
        auto duN = evaluateGradient( shapes, locationMap, solutionDofs )[0];

        targets[0][0] += weightDetJ * ( uN * uN + duN * duN );
        targets[1][0] += weightDetJ * ( uA * uA + duA * duA );
        targets[2][0] += weightDetJ * ( ( uN - uA ) * ( uN - uA ) + ( duN - duA ) * ( duN - duA ) );
    };

    return DomainIntegrand<2>( DiffOrders::FirstDerivatives, 
        std::vector( 3, AssemblyType::Scalar ), evaluate );
}

auto doMain( size_t n, size_t p, bool petrov, bool writeVtu )
{
    static constexpr size_t D = 1;

    auto nelements = array::make<size_t, D + 1>( n );
    auto lengths = array::make<double, D + 1>( 1.0 );
    auto nsamples = array::make<size_t, D + 1>( 20 );

    auto capacity = spatial::constantFunction<D + 1>( 1.0 );
    auto conductivity = spatial::constantFunction<D + 1>( 0.01 );
    
    // Analytical solution / source
    std::function solution = [=]( std::array<double, D + 1> xt )
    {
        double alpha = (2.0 * 2 + 0.5) * std::numbers::pi;
        double u1 = std::sin( alpha * xt[0] ) * std::exp( -alpha * alpha * 0.01 * xt[1] );

        double u2 = 0.25 * std::sin(4 * std::numbers::pi * xt[1]);

        return u1 + u2;
    };

    std::function derivative = [=]( std::array<double, D + 1> xt )
    {
        double alpha = (2.0 * 2 + 0.5) * std::numbers::pi;

        return alpha * std::cos(alpha * xt[0]) * std::exp(-alpha * alpha * 0.01 * xt[1]);
    };

    auto source = [=]( std::array<double, D + 1> xt )
    {
        return std::numbers::pi * std::cos(4 * std::numbers::pi * xt[1]);
    };

    // Make and refine grid
    auto grid = makeRefinedGrid( nelements, lengths );

    // Create multi-level hp basis on refined grid
    auto basis_ = makeHpBasis<TensorSpace>( grid, p );

    auto filteredMesh = std::make_shared<FilteredMesh<D + 1>>( grid, CellIndexVector { } );
    auto basis = std::make_shared<ElementFilterBasis<D + 1>>( basis_, filteredMesh );

    // Allocate sparse matrix
    auto boundaryDofs = boundary::boundaryDofs<D + 1>(solution, *basis, { boundary::face( 0, 0 ), boundary::face( 1, 0 ) } );

    auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, boundaryDofs.first );
    auto vector = std::vector<double>( matrix.size1( ), 0.0 );

    // Assemble global system
    auto integrand = makeIntegrand<D>( capacity, conductivity, source, *basis, petrov );

    integrateOnDomain( *basis, integrand, { matrix, vector }, boundaryDofs );

    // Solve global system
    auto interiorSolution = pardisoSolve( matrix, vector );
    auto fullSolution = boundary::inflate( interiorSolution, boundaryDofs );
 
    // Integrate errors
    auto errorIntegrand1 = makeL2ErrorIntegrand( fullSolution, solution );
    auto errorIntegrand2 = makeL2H1ErrorIntegrand( fullSolution, solution, derivative );

    auto l2Integrals = ErrorIntegrals { };
    auto l2H1Integrals = ErrorIntegrals { };

    integrateOnDomain( *basis, errorIntegrand1, l2Integrals );
    integrateOnDomain( *basis, errorIntegrand2, l2H1Integrals );

    if( writeVtu )
    {
        auto postprocessNumericalSolution = makeSolutionPostprocessor<D + 1>( fullSolution, 1 );
        auto postprocessAnalyticalSolution = makeFunctionPostprocessor<D + 1>( solution, "AnalyticalSolution" );
  
        postprocess( nsamples, { postprocessNumericalSolution, postprocessAnalyticalSolution },
                     *basis, "outputs/spacetime_convergence" );
    }

    std::cout << n << " " << p << " " << petrov << " " << interiorSolution.size( ) 
              << " " << l2Integrals.analytical( ) << " " << l2Integrals.relativeDifference( ) 
              << " " << l2H1Integrals.analytical( ) << " " << l2H1Integrals.relativeDifference( ) << std::endl;
}

} // namespace mlhp

int main( int argn, char** argv )
{
    bool writeVtu = false;

    if( argn == 6 )
    {
        writeVtu = std::string( argv[5] ) == "true";

        argn -= 1;
    }

    if( argn != 5 )
    {
        std::cout << 
            "Invalid number of arguments: " << std::endl <<
            "spacetime_convergence begin end degree petrov (optional) writeVtu" << std::endl;

        return 1;
    }

    size_t beginStudy = static_cast<size_t>( std::stoi( argv[1] ) );
    size_t endStudy = static_cast<size_t>( std::stoi( argv[2] ) );
    size_t degree = static_cast<size_t>( std::stoi( argv[3] ) );
    size_t petrov = std::string( argv[4] ) == "true";


    std::cout << "Parameters: study from " << beginStudy << " to " << endStudy << ", p = " << degree
        << ", petrov = " << petrov << ", writeVtu = " << writeVtu << std::endl << std::endl;

    std::cout << "NElements Degree Petrov NDof L2NormAnalytical RelativeL2Error L2H1NormAnalytical RelativeL2H1Error" << std::endl;

    for( size_t i = beginStudy; i <= endStudy; ++i )
    {
        size_t n = mlhp::utilities::integerPow( 2, i );

        mlhp::doMain( n, degree, petrov, writeVtu );
    }
}