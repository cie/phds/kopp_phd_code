The code repository associated to the phd thesis: 

https://gitlab.lrz.de/cie/phds/kopp_phd_thesis.

Contains the examples from the paper repositories 
- https://gitlab.com/hpfem/publications/2021_nd-mlhp
- https://gitlab.com/hpfem/publications/2021_spacetime_am

with additional examples for Chapter 2 of the thesis. All codes use 

https://gitlab.com/phmkopp/mlhp

as git submodule.

