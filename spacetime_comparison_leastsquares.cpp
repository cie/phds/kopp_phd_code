// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "helper_mkl.hpp"
#include "helper_comparison.hpp"

namespace mlhp
{

template<size_t D>
auto extractSizes( const BasisFunctionEvaluation<D + 1>& shapes )
{
    std::array<size_t, D + 1> ndofs { };
    std::array<size_t, D + 1> padded { };
    std::array<size_t, D + 2> offsets { };

    for( size_t ifield = 0; ifield < D + 1; ++ifield )
    {
        ndofs[ifield] = shapes.ndof( ifield );
        padded[ifield] = shapes.ndofpadded( ifield );
        offsets[ifield + 1] = offsets[ifield] + ndofs[ifield];
    }

    auto allpadded = memory::paddedLength<double>( offsets.back( ) );

    return std::make_tuple( ndofs, padded, offsets, allpadded );
}

template<size_t D>
auto makeLeastSquaresSpaceTimeIntegrand( const NonlinearHeatMaterial& material,
                                         const spatial::ScalarFunction<D + 1>& source,
                                         const std::vector<double>& dofs, bool jacobian,
                                         const AbsBasis<D + 1>& basis )
{
    auto evaluate = [=]( const BasisFunctionEvaluation<D + 1>& shapes,
                         const LocationMap& locationMap,
                         AlignedDoubleVectors& targets,
                         AlignedDoubleVector&,
                         double weightDetJ) 
    { 

        double f = source( shapes.xyz( ) );
        
        auto solution = evaluateSolutions<D + 1, D + 1>( shapes, locationMap, dofs );
        auto derivatives = evaluateGradients<D + 1, D + 1>( shapes, locationMap, dofs );

        auto [sigma, u] = array::peel(solution, 0);
        auto [dsigma, du] = array::peel(derivatives, 0);

        auto [c, cPrime, k, kPrime] = material( u );

        auto lhs = memory::assumeAlignedNoalias( targets[0].data( ) );
        auto rhs = jacobian ? targets[1].data( ) : targets[0].data( );

        auto [ndofs, padded, offsets, allpadded] = extractSizes<D>( shapes );

        double beta = std::pow( 800.0, 2 );

        auto ures = c * du[D] - f;
        auto sres = std::array<double, D>{ };

        for( size_t axis = 0; axis < D; ++axis )
        {
            ures -= dsigma[axis][axis];
            sres[axis] = k * du[axis] - sigma[axis];
        }

//        sres = sres * beta;

        // Right hand side
        {
            auto dN0 = shapes.noalias( 0, 1 );

            for( size_t idof = 0; idof < ndofs[0]; ++idof )
            {
                double value = ures * c * dN0[D * padded[0] + idof];

                for( size_t axis = 0; axis < D; ++axis )
                {
                    value += beta * sres[axis] * k * dN0[axis * padded[0] + idof];
                }

                rhs[idof] += value * weightDetJ;
            } 

            for( size_t iblock = 0; iblock < D; ++iblock )
            {
                auto [Ni, dNi] = shapes.template noalias<1>( iblock + 1 );

                for( size_t idof = 0; idof < ndofs[iblock + 1]; ++idof )
                {
                    double value = -beta * sres[iblock] * Ni[idof] - ures * dNi[iblock * padded[iblock + 1] + idof];

                    rhs[offsets[iblock + 1] + idof] += value * weightDetJ; //-dNi[iblock * padded[iblock + 1] + idof] * f * weightDetJ;
                }
            }
        }

        if( !jacobian ) return;

        // Upper left
        {
            auto [N, dNPtr] = shapes.template noalias<1>( 0 );

            auto dN = [&](size_t axis) { return dNPtr + axis * padded[0]; };

            for( size_t idof = 0; idof < ndofs[0]; ++idof )
            {
                for( size_t jdof = 0; jdof < ndofs[0]; ++jdof )
                {
                    double value = dN(D)[idof] * (c * c * dN(D)[jdof] + (ures + c * du[D]) * cPrime * N[jdof]);

                    for( size_t axis = 0; axis < D; ++axis )
                    {
                        value += beta * dN(axis)[idof] * (k * k * dN(axis)[jdof] + (sres[axis] + k * du[axis]) * kPrime * N[jdof]);
                    } // axis

                    lhs[idof * allpadded + jdof] += value * weightDetJ;

                } // jdof
            } // idof
        } 

        // Upper right row
        for( size_t jblock = 0; jblock < D; ++jblock )
        {
            auto [Ni, dNiPtr] = shapes.template noalias<1>( 0 );
            auto [Nj, dNjPtr] = shapes.template noalias<1>( jblock + 1 );

            auto dNi = [&](size_t axis) { return dNiPtr + axis * padded[0]; };
            auto dNj = [&](size_t axis) { return dNjPtr + axis * padded[jblock + 1]; };

            for( size_t idof = 0; idof < ndofs[0]; ++idof )
            {
                for( size_t jdof = 0; jdof < ndofs[jblock + 1]; ++jdof )
                {
                    double value = - c * dNi(D)[idof] * dNj(jblock)[jdof] - k * beta * dNi(jblock)[idof] * Nj[jdof];

                    lhs[idof * allpadded + offsets[jblock + 1] + jdof] += value * weightDetJ;

                } // jdof
            } // idof
        }

        // Lower left column
        for( size_t iblock = 0; iblock < D; ++iblock )
        {
            auto [Ni, dNiPtr] = shapes.template noalias<1>( iblock + 1 );
            auto [Nj, dNjPtr] = shapes.template noalias<1>( 0 );

            auto dNi = [&](size_t axis) { return dNiPtr + axis * padded[iblock + 1]; };
            auto dNj = [&](size_t axis) { return dNjPtr + axis * padded[0]; };

            for( size_t idof = 0; idof < ndofs[iblock + 1]; ++idof )
            {
                for( size_t jdof = 0; jdof < ndofs[0]; ++jdof )
                {
                    double value = -dNi(iblock)[idof] * (c * dNj(D)[jdof] + cPrime * Nj[jdof] * du[D]) 
                                   -Ni[idof] * beta * (k * dNj(iblock)[jdof] + kPrime * Nj[jdof] * du[iblock]);   

                    lhs[(idof + offsets[iblock + 1]) * allpadded + jdof] += value * weightDetJ;

                } // jdof
            } // idof
        }

        // Lower right block
        for( size_t iblock = 0; iblock < D; ++iblock )
        {
            for( size_t jblock = 0; jblock < D; ++jblock )
            {
                auto [Ni, dNi] = shapes.template noalias<1>( iblock + 1 );
                auto [Nj, dNj] = shapes.template noalias<1>( jblock + 1 );

                for( size_t idof = 0; idof < ndofs[iblock + 1]; ++idof )
                {
                    double factor = iblock == jblock ? beta * Ni[idof] : 0.0; 

                    for( size_t jdof = 0; jdof < ndofs[jblock + 1]; ++jdof )
                    {
                        double value = factor * Nj[jdof] + dNi[iblock * padded[iblock + 1] + idof] * 
                                                           dNj[jblock * padded[jblock + 1] + jdof];

                        lhs[( idof + offsets[iblock + 1] ) * allpadded + offsets[jblock + 1] + jdof] += value * weightDetJ;

                    } // jdof
                } // idof
            } 
        }
    };

    auto types = jacobian ? std::vector { AssemblyType::UnsymmetricMatrix, AssemblyType::Vector } :
                            std::vector { AssemblyType::Vector };

    return DomainIntegrand<D + 1>( DiffOrders::FirstDerivatives, types, evaluate );
}


template<size_t D>
auto leastSquaresPhaseChangeIntegrandFactory( NonlinearHeatMaterial material,
                                              PhaseChangeParameters phaseChange,
                                              const spatial::ScalarFunction<D + 1>& source )
{
    return [=](const AbsBasis<D + 1>& basis, const std::vector<double>& dofs, size_t iteration, bool computeJacobian)
    { 
        if( computeJacobian && iteration == 0 )
        {
            return makeLeastSquaresSpaceTimeIntegrand<D>( material, source, dofs, computeJacobian, basis );
        }
        else
        {
            auto model = phaseChange;

            model.S = iteration < 2 ? std::max( 10.0, model.S ) : model.S;

            auto phaseChangeMaterial = makePhaseChangeMaterial( material, model );

            return makeLeastSquaresSpaceTimeIntegrand<D>( phaseChangeMaterial, source, dofs, computeJacobian, basis );
        }
    };
}

struct Compute : public Comparison
{
    using Comparison::Comparison;

    void compute( )
    {
        auto refinementStrategy = laser::makeRefinement<D>( laserTrack, refinements );

        auto mesh = makeRefinedGrid( array::insert( nelements, D, size_t { nslabs } ),
                                     array::insert( lengths, D, duration ),
                                     array::insert( origin, D, 0.0 ) );
    
        mesh->refine( refinementStrategy );

        auto grading = std::function { [=, this]( const AbsHierarchicalGrid<D + 1>& grid, size_t nfields )
        {
            AnsatzTemplateVector ansatzTemplates( { grid.ncells( ), nfields, D + 1 }, 0 );

            for( size_t ielement = 0; ielement < ansatzTemplates.shape( )[0]; ++ielement )
            {
                for( size_t axis = 0; axis < D; ++axis )
                {
                    ansatzTemplates( ielement, 0, axis ) = degree;

                    for( size_t ifield = 0; ifield < D; ++ifield )
                    {
                        ansatzTemplates( ielement, ifield + 1, axis ) = std::max( degree, PolynomialDegree { 2 } ) - 1;
                    }
                }

                for( size_t ifield = 0; ifield < D + 1; ++ifield )
                {
                    ansatzTemplates( ielement, ifield, D ) = 1;
                }
            }

            return ansatzTemplates;
        } };

        auto mlhpBasis = makeHpBasis<TrunkSpace>( mesh, grading, D + 1 );

        auto filteredMesh = std::make_shared<FilteredMesh<D + 1>>(mesh, CellIndexVector { });
        auto basis = std::make_shared<ElementFilterBasis<D + 1>>(mlhpBasis, filteredMesh);

        std::cout << "Least-squares formulation:" << std::endl;
        std::cout << "duration :" << duration * 1000 << std::endl;
        std::cout << "nslabs: " << nslabs << std::endl;
        std::cout << "ndof: " << basis->ndof( ) << std::endl;

        // Assembly
        auto t0 = utilities::tic( "Assembly ... " );

        auto integrandFactory = leastSquaresPhaseChangeIntegrandFactory<D>(material, phaseChange, source);

        auto initialTemperature = spatial::constantFunction<D + 1>( 25.0 );

        //auto dirichletDofs0 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 0 );
        //auto dirichletDofs1 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 1 );
        //auto dirichletDofs2 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 2 );
        //auto dirichletDofs3 = boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 3 );

        std::vector<DofIndicesValuesPair> dirichletFaceDofs =
        {
            boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::face( D, 0 ) }, 0 ),
            //boundary::boundaryDofs<D + 1>( initialTemperature, *basis, { boundary::allFaces( D ) }, 0 )
        };

        for( size_t axis = 0; axis < D; ++axis )
        {
            dirichletFaceDofs.push_back( boundary::boundaryDofs<D + 1>( spatial::constantFunction<D + 1>( 0.0 ), *basis,
                { boundary::face( D, 0 ), boundary::face( axis, 0 ), boundary::face( axis, 1 ) }, axis + 1 ) );
        }

        auto dirichletDofs = boundary::combine( dirichletFaceDofs );

        auto dofs = solveNonlinearProblemBisection<D>( *basis, dirichletDofs, integrandFactory, { 1, 0 } );

//        auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, dirichletDofs.first );
//        auto rhs = std::vector<double>( matrix.size1( ) );
//
//        integrateOnDomain( *basis, integrand, { matrix, rhs }, dirichletDofs );
//
//        utilities::toc( t0, "Took: " );
//
//        // Linear solution
//        auto t1 = utilities::tic( "Solving ... " );
//        //auto solution = boundary::inflate( pardisoSolve( matrix, rhs ), dirichletDofs );
//        auto solution = boundary::inflate( makeMklCGSolver( )( matrix, rhs ), dirichletDofs );
//        //auto solution = boundary::inflate( linalg::makeCGSolver( )( matrix, rhs ), dirichletDofs );
//        utilities::toc( t1, "Took: " );

        // Postprocessing
        auto nsamples = array::add<size_t>( array::make<size_t, D>( degree ), 4 );

        std::vector postprocessors =
        {
            makeSolutionPostprocessor<D + 1>( dofs, D + 1 ),
            makeFunctionPostprocessor<D + 1>( source, "Source" ),
        };

        postprocessSlabSlices<D>( nsamples, postprocessors, *basis, "outputs/spacetime_formulation_leastsquares", 0, nslabs, 4 );

    } // Compute::compute
};

} // namespace mlhp

int main( int argc, char** argv )
{
    mlhp::Compute { false, 1 }.compute( );
}

