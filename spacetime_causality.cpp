// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"
#include "helper_comparison.hpp"

namespace mlhp
{

template<size_t D>
auto makeIntegrand( const spatial::ScalarFunction<D + 1>& capacity,
                    const spatial::ScalarFunction<D + 1>& conductivity,
                    const spatial::ScalarFunction<D + 1>& source,
                    const AbsBasis<D + 1>& basis,
                    bool petrov = true )
{

    auto evaluate = [=, &basis]( const BasisFunctionEvaluation<D + 1>& shapes,
                                 AlignedDoubleVectors& targets, double weightDetJ )
    { 
        auto ndof = shapes.ndof( );
        auto nblocks = shapes.nblocks( );
        auto ndofpadded = shapes.ndofpadded( );

        double c = capacity( shapes.xyz( ) );
        double k = conductivity( shapes.xyz( ) );
        double f = source( shapes.xyz( ) );

        auto& testfilter = createTestFilter<D>( basis, shapes, petrov );      

        auto trialN = shapes.noalias( 0, 0 );
        auto trialDN = shapes.noalias( 0, 1 );

        auto testN = testFunctions<D>( shapes, petrov );
        auto testDN = testGradient<D>( shapes, petrov );

        linalg::elementRhs( targets[1].data( ), ndof, nblocks, [&](size_t i)
        { 
            return testN[i] * f * weightDetJ * testfilter[i];
        } );     

        linalg::unsymmetricElementLhs( targets[0].data( ), ndof, nblocks, [=]( size_t i, size_t j )
        { 
            double value = c * testN[i] * trialDN[D * ndofpadded + j];

            for( size_t axis = 0; axis < D; ++axis )
            {
                value += k * testDN[axis][i] * trialDN[axis * ndofpadded + j];
            }

            return value * weightDetJ * testfilter[i];
        } );
    };

    auto diffOrder = petrov ? DiffOrders::SecondDerivatives : DiffOrders::FirstDerivatives;
    auto types = std::vector { AssemblyType::UnsymmetricMatrix, AssemblyType::Vector };

    return DomainIntegrand<D + 1>( types, diffOrder, evaluate );
}

auto doMain( double sigma, bool petrov )
{
    static constexpr size_t D = 1;

    auto nelements = array::make<size_t, D + 1>( 16 );
    auto lengths = array::make<double, D + 1>( 1.0 );
    auto p = size_t { 1 };
    auto nsamples = array::make<size_t, D + 1>( 4 );

    auto capacity = spatial::constantFunction<D + 1>( 1.0 );
    auto conductivity = spatial::constantFunction<D + 1>( 0.01 );
    
    std::function source = spatial::integralNormalizedGaussBell<D + 1>( { 0.5, 0.5 }, sigma );

    // Make and refine grid
    auto grid = makeRefinedGrid( nelements, lengths );

    // Create multi-level hp basis on refined grid
    auto basis_ = makeHpBasis<TensorSpace>( grid, p );

    auto filteredMesh = std::make_shared<FilteredMesh<D + 1>>( grid, CellIndexVector { } );
    auto basis = std::make_shared<ElementFilterBasis<D + 1>>( basis_, filteredMesh );

    // Allocate sparse matrix
    auto boundaryDofs = boundary::boundaryDofs<D + 1>( spatial::constantFunction<D + 1>( 0.0 ), 
        *basis, { boundary::face( 0, 0 ), boundary::face( 1, 0 ) } );

    auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, boundaryDofs.first );
    auto vector = std::vector<double>( matrix.size1( ), 0.0 );

    // Assemble global system
    auto integrand = makeIntegrand<D>( capacity, conductivity, source, *basis, petrov );
    
    auto quadrature = makeIntegrationOrderDeterminor<D + 1>( 10 );

    integrateOnDomain( *basis, integrand, { matrix, vector }, quadrature, boundaryDofs );

    // Solve global system
    auto interiorSolution = pardisoSolve( matrix, vector );
    auto fullSolution = boundary::inflate( interiorSolution, boundaryDofs );

    auto postprocessNumericalSolution = makeSolutionPostprocessor<D + 1>( fullSolution, 1 );
    auto postprocessSource = makeFunctionPostprocessor<D + 1>( source, "Source" );
  
    postprocess( nsamples, { postprocessNumericalSolution, postprocessSource },
                    *basis, "outputs/spacetime_causality" );
}

} // namespace mlhp

int main( int argn, char** argv )
{
    if( argn != 3 )
    {
        std::cout << 
            "Invalid number of arguments: " << std::endl <<
            "spacetime_convergence sigma petrov (optional) writeVtu" << std::endl;

        return 1;
    }

    double sigma = std::stod( argv[1] );
    size_t petrov = std::string( argv[2] ) == "true";

    std::cout << "Parameters: sigma = " << sigma << ", petrov = " << petrov << std::endl;

    mlhp::doMain( sigma, petrov );
}