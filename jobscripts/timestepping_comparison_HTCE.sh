#!/bin/bash

# Usage, for example:
# ./scheduleComparisonHTCE.sh short parallel cg unsymmetric 2 5

partition=$1
parallel=$2
solver=$3
symmetric=$4
start=$5
end=$6

parallelShort="${parallel:0:3}"
solverShort="${solver:0:2}"
symmetricShort="${symmetric:0:1}"

studyStr="Ts${parallelShort^[p,s]}${solverShort^[p,c]}${symmetricShort^[s,u]}"
studyStrRange="${studyStr}_${start}-${end}"

filename="schedule"$studyStr".cmd"

str=""

str+="#!/bin/bash                                                              \n"
str+="                                                                         \n"
str+="#SBATCH -J "$studyStr"                                                   \n"
str+="#SBATCH -o /dss/dsshome1/lxc09/ga49sos2/log/comparison/$studyStrRange.out\n"
str+="#SBATCH -D ./                                                            \n"
str+="#SBATCH --get-user-env                                                   \n"
str+="#SBATCH --clusters=htce                                                  \n"
str+="#SBATCH --partition=htce_"$partition"                                    \n"
str+="#SBATCH --reservation=htce_users                                         \n"
str+="#SBATCH --exclusive                                                      \n"
str+="#SBATCH --cpus-per-task=40                                               \n"
str+="#SBATCH --mail-type=end                                                  \n"
str+="#SBATCH --mail-user=philipp.kopp@tum.de                                  \n"
str+="                                                                         \n"

if [ "${parallel}" == "parallel" ]; then
    str+="cd ~/phd/build                                                       \n"
    str+="export OMP_NUM_THREADS=20                                            \n"
else
    str+="cd ~/phd/serial                                                      \n"
    str+="export OMP_NUM_THREADS=1                                             \n"
fi
str+="./bin/timestepping_comparison "$parallel" "$solver" "$symmetric" "$start" "$end"  \n"


#echo -e ${str}
echo -e ${str} > ${filename}

sbatch ${filename}

rm ${filename}
