// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "helper_spacetime.hpp"
#include "helper_am.hpp"

namespace mlhp
{

struct Compute : public Units
{
    void compute( )
    {
        static constexpr size_t D = 3;

        double S = 1.0;
        double L = 2.8e5 * J / kg;
        double TlOffset = 0;// 200 * C;

        size_t factor = 6;

        // Domain, base mesh and initial condition
        auto lengths = std::array<double, D> { 1.1875 * mm, 0.5 * mm, 0.2 * mm };
        auto nelements = std::array<size_t, D> { 19 * factor, 8 * factor, 6 * factor };
        auto nslabs = size_t { 16 * factor };
        auto initialTemperature = 25.0 * C;

        // Laser model
        double laserSpeed = 800.0 * mm / s;
        double laserD4Sigma = 170.0 * um;
        double laserPower = 179.2 * W;
        double laserAbsorptivity = 0.32; 
        double laserDepthScaling = 0.28;

        // Laser source 
        CoordinateList<D> laserPositions
        {
            {  1.012 * mm, lengths[1] / 2.0, lengths[2] },
            {  0.137 * mm, lengths[1] / 2.0, lengths[2] }
        };

        auto laserTrack = laser::makeTrack( laserPositions, laserSpeed, laserPower );
        auto source = laser::volumeSource<D>( laserTrack, laserD4Sigma, laserAbsorptivity, laserDepthScaling );
        auto duration = laserTrack.back( ).time;

        // Material
        double density = 8440.0 * kg / ( m * m * m );
 
        auto material = [=, this]( double T0 ) 
        { 
            double T = std::min( T0, 1290.0 );
            double dT = T0 < 1290.0 ? 1.0 : 0.0;

            //T = 200;
            //dT = 0;

            return NonlinearHeatParameters
            {
                .specificCapacity      = ( 2.47e-4 * T + 4.05e-1 ) * J / ( g * C ) * density,
                .specificCapacityPrime = ( 2.47e-4 * dT          ) * J / g         * density,
                .conductivity          = ( 1.50e-4 * T + 9.50e-2 ) * W / ( cm * C ),
                .conductivityPrime     = ( 1.50e-4 * dT          ) * W / cm
            }; 
        };

        auto phaseChange = PhaseChangeParameters
        {
            .density               = density,
            .latentHeat            = L,
            .liquidConductivity    = 0.0, 
            .Ts                    = 1290.0 * C,
            .Tl                    = 1350.0 * C + TlOffset,
            .S                     = S
        };

        auto integrandFactory = phaseChangeIntegrandFactory<D>( material, phaseChange, source );

        // h- and p-refinement
        auto spatialDegrees = { 3, 3 };
        auto temporalDegrees = { 1, 1 };

        auto grading = perLevelGrading<D>( spatialDegrees, temporalDegrees );

        // Refinement based on laser path (delay, sigma, level, zfactor)
        std::vector refinements =
        {
            laser::Refinement { 0.0 * ms, 0.12 * mm, 0, 0.5 },
            //laser::Refinement { 1.2 * ms, 0.18 * mm, 0, 0.5 },
        };

         // Problem setup 
        auto baseGrid = CartesianGrid<D + 1>( array::insert( nelements, D, nslabs ),
                                              array::insert( lengths, D, duration ) );

        auto refinementStrategy = laser::makeRefinement<D>( laserTrack, refinements );

        size_t slabSize = 1;
        bool useTrunkSpace = true;

        SlabState<D> slab( baseGrid, slabSize );

        for( slab.islab = 0; slab.islab < slab.nslabs( ); ++slab.islab )
        {
            std::cout << "Computing slab " << slab.islab + 1 << " / " << slab.nslabs( );

            // Create one grid including ghost slabs and then filter ghost cells
            auto unfilteredMesh = createWithGhostCells<D>( baseGrid, slab.cell0( ), slab.cell1( ) );

            unfilteredMesh->refine( refinementStrategy );

            auto mesh = filterGhostCells<D>( unfilteredMesh, slab.islab, slab.nslabs( ) );

            // Create basis on unfiltered grid and then remove ghost elements
            auto unfilteredBasis = makeSpaceTimeTrunkSpace<D>( unfilteredMesh, grading );

            auto basis = std::make_shared<ElementFilterBasis<D + 1>>( unfilteredBasis, mesh );

            // Compute initial slab dofs
            if( slab.islab == 0 )
            {
                auto temperatureFunction = spatial::constantFunction<D + 1>( initialTemperature );

                slab.dirichletDofs = boundary::boundaryDofs<D + 1>( temperatureFunction, *basis, { boundary::face( D, 0 ) } );
            }
            else
            {
                auto boundaryFaces = mesh::boundaries( *mesh, { boundary::face( D, 0 ) } )[0];

                slab.dirichletDofs.first = boundary::boundaryDofIndices<D + 1>( *basis, boundaryFaces );
            }

            // Log number of dofs and solve system
            auto nboundary = slab.dirichletDofs.first.size( );
            auto ninternal = basis->ndof( ) - nboundary;

            std::cout << " (" << ninternal << " internal and " << nboundary << " boundary dofs)" << std::endl;

            auto dofs = solveNonlinearProblemBisection<D>( *basis, slab.dirichletDofs, integrandFactory, { 1, 0 } );
            
//            size_t interval = slab.nslabs( ) / 8;
//            if( slab.islab / slab.nslabs( ) % interval == 0 )
//            {
//                slab.islab /= interval;

                auto phaseChangeMaterial = makePhaseChangeMaterial( material, phaseChange );

                auto function = [&]( const BasisFunctionEvaluation<D + 1>& shapes,
                                        const DofIndexVector& locationMap,
                                        std::span<double> target )
                {
                    auto u = evaluateSolution(shapes, locationMap, dofs);

                    auto [c, cPrime, k, kPrime] = material(u);
                    auto [rho, L, K, Ts, Tl, S] = phaseChange;
                    auto [fpc, dfpc, ddfpc] = regularizedStepFunction(Ts, Tl, u, S);

                    c += L * dfpc * rho;
                    //k += K * fpc;
                    cPrime += L * ddfpc * rho;
                    //kPrime += K * dfpc;

                    target[0] = c;
                    target[1] = cPrime;
                    target[2] = fpc;
                    target[3] = dfpc;
                    target[4] = ddfpc;
                };

                // Postprocess solution
                std::vector postprocessors = 
                { 
                    makeSolutionPostprocessor<D + 1>( dofs, 1 ),
                    makeDerivativePostprocessor<D + 1>( dofs, D, "TimeDerivative" ),
                    makeFunctionPostprocessor( source, "Source" ),
                    makeRefinementLevelFunctionPostprocessor<D>( laserTrack, refinements ),
                    ElementPostprocessor<D + 1>( function, 5, 1, "PhaseChange" )
                };
            
                // Postprocess with 6 subcells per element and 4 slices in time per base cell
                postprocessSlabSlices<D>( array::make<size_t, D>( 5 ), postprocessors, *basis, 
                   "outputs/spacetime_AMB2018-02_coarse", slab.cell0( ), slab.cell1( ), 1 );

//                slab.islab *= interval;
//            }

            // Extract dofs for next slab
            auto boundaryFaces = mesh::boundaries( *mesh, { boundary::face( D, 1 ) } )[0];

            slab.dirichletDofs.first = boundary::boundaryDofIndices<D + 1>( *basis, boundaryFaces );
            slab.dirichletDofs.second = algorithm::extract( dofs, slab.dirichletDofs.first );

        } // for islab
    } // Compute::compute
};

} // namespace mlhp

int main( )
{
    mlhp::Compute { }.compute( );
}

