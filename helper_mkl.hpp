#include <mkl.h>

#ifndef HELPER_MKL_HPP
#define HELPER_MKL_HPP

#include "mlhp/core.hpp"

namespace mlhp
{
namespace detail
{

class MklSparseMatrix
{
    sparse_matrix_t matrix;
    matrix_descr descriptor;

    void checkErrorCode( auto code, std::string step ) 
    { 
        MLHP_CHECK( code == SPARSE_STATUS_SUCCESS, "MKL error in " + step + " step (code " + std::to_string( code ) + ")." );
    }

public:

    MklSparseMatrix( const linalg::AbsSparseMatrix& A, 
                     sparse_matrix_type_t type, 
                     sparse_fill_mode_t mode ) 
    {
        descriptor.type = type;
        descriptor.mode = mode;
        descriptor.diag = SPARSE_DIAG_NON_UNIT;
  
        static_assert( sizeof( MKL_INT ) == sizeof( *A.indptr( ) ), "Inconsistent integer sizees." );
        static_assert( sizeof( MKL_INT ) == sizeof( *A.indices( ) ), "Inconsistent integer sizees." );

        // Initialize matrix structure
        checkErrorCode( mkl_sparse_d_create_csr( &matrix, SPARSE_INDEX_BASE_ZERO, A.size1( ), A.size2( ), 
            const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( A.indptr( ) ) ), 
            const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( A.indptr( ) + 1 ) ), 
            const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( A.indices( ) ) ), 
            const_cast<double*>( A.data( ) ) ), "initialization" );
    
//        // Optimize for multiple calls
//        checkErrorCode( mkl_sparse_set_mv_hint( matrix, SPARSE_OPERATION_NON_TRANSPOSE, descriptor, 200 ), "mv_hint" );
//        checkErrorCode( mkl_sparse_optimize( matrix ), "optimize" );
    }

    // Compute y =  alpha * A * x + beta * y
    void compute( double alpha, const double* x, double beta, double* y )
    {
        checkErrorCode( mkl_sparse_d_mv( SPARSE_OPERATION_NON_TRANSPOSE, alpha, matrix, descriptor, x, beta, y ), "multiplication" );
    }

    ~MklSparseMatrix( ) //noexcept( false )
    {
        //checkErrorCode( mkl_sparse_destroy( matrix ), "cleanup" );
        mkl_sparse_destroy( matrix );
    }
}; 

}
  
auto makeMklMultiply( const linalg::SymmetricSparseMatrix& M )
{
    auto multiply = std::make_shared<detail::MklSparseMatrix>( M, SPARSE_MATRIX_TYPE_SYMMETRIC, SPARSE_FILL_MODE_UPPER );

    return std::function { [=]( const double* x, double* y ){ multiply->compute( 1.0, x, 0.0, y ); } };
}

auto makeMklMultiply( const linalg::UnsymmetricSparseMatrix& M )
{
    auto multiply = std::make_shared<detail::MklSparseMatrix>( M, SPARSE_MATRIX_TYPE_GENERAL, SPARSE_FILL_MODE_UPPER );

    return std::function { [=]( const double* x, double* y ){ multiply->compute( 1.0, x, 0.0, y ); } };
}

auto makeMklCGSolverWithInfo( double tolerance = 1e-8 )
{
    auto info = std::make_shared<linalg::IterativeSolverInfo>( );

    return std::pair { std::function { [=]( const linalg::AbsSparseMatrix& M,
                                            const std::vector<double>& f )
    {
        auto type = M.symmetricHalf( ) ? SPARSE_MATRIX_TYPE_SYMMETRIC : SPARSE_MATRIX_TYPE_GENERAL;
        auto sharedMultiply =  std::make_shared<detail::MklSparseMatrix>( M, type, SPARSE_FILL_MODE_UPPER );
        auto multiplyFunction = [=]( const double* x, double* y ){ sharedMultiply->compute( 1.0, x, 0.0, y ); };
        auto P = linalg::makeDiagonalPreconditioner( M );

        std::vector<double> x;

        info->residualNorms_ = linalg::cg( multiplyFunction, f, x, P, M.size1( ), tolerance );

        return x;
    } }, info };
}

auto makeMklCGSolver( double tolerance = 1e-8 )
{
    return makeMklCGSolverWithInfo( tolerance ).first;
}


auto pardisoSolve2( const linalg::AbsSparseMatrix& matrix,
                    const std::vector<double>& rhs,
                    bool SPD = false )
{
    void* internalSolverMemoryPointer[64];

    MKL_INT iparm[64];
    MKL_INT maximuNumberOfFactorization = 1;
    MKL_INT matrixNumber = 1;
    MKL_INT matrixType = SPD ? 2 : 11;
    MKL_INT numberOfRightHandSides = 1;
    MKL_INT n = static_cast<MKL_INT>( rhs.size( ) );
    MKL_INT error = 0;
    MKL_INT integerDummy = 0;
    MKL_INT messageLevel = 0;

    std::fill( internalSolverMemoryPointer, internalSolverMemoryPointer + 64, nullptr );
    std::fill( iparm, iparm + 64, 0 );

    PARDISOINIT( internalSolverMemoryPointer, &matrixType, iparm );

    iparm[1] = 3;  // fill in reducing ordering: 3-> parallel version of metis
    //iparm[9] = 8; // Pivoting perturbation : 10 ^ -8
    iparm[23] = 1; // Parallel factorization control: 1->improved two-level factorization algorithm
    iparm[26] = 1; // Enable matrix consistency checks
    iparm[34] = 1; // One- or zero-based indexing of columns and rows: 1->zero based

    enum class PardisoPhase : MKL_INT
    {
        REORDER = 11, FACTORIZATION = 22, SOLUTION = 33, CLEANUP = -1
    };
    
    auto callPardiso = [&]( PardisoPhase phase, double* data, MKL_INT* indices, 
                            MKL_INT* indptr, double* right, double* solution )
    {
        MKL_INT phaseInt = static_cast<MKL_INT>( phase );

        PARDISO_64( internalSolverMemoryPointer, &maximuNumberOfFactorization, &matrixNumber, 
                    &matrixType, &phaseInt, &n, data, indptr, indices, &integerDummy, 
                    &numberOfRightHandSides, iparm, &messageLevel, right, solution, &error );

        if( error != 0 ) std::cout << "Error in pardiso solver." << std::endl;
    };

    MLHP_CHECK( -1 == ~0, "Reinterpret cast to signed requires two's complement." );
    MLHP_CHECK( sizeof( linalg::SparsePtr ) == sizeof( MKL_INT ), "Inconsistent integer sizes." );
    MLHP_CHECK( sizeof( linalg::SparseIndex ) == sizeof( MKL_INT ), "Inconsistent integer sizes." );

    // Reinterpret cast to signed works if machine uses two's complement (as tested above).
    auto* i = const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( matrix.indices( ) ) );
    auto* j = const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( matrix.indptr( ) ) );
    auto* v = const_cast<double*>( matrix.data( ) );
    auto* f = const_cast<double*>( rhs.data( ) );

    std::vector<double> solution( rhs.size( ) );

    callPardiso( PardisoPhase::REORDER, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::FACTORIZATION, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::SOLUTION, v, i, j, f, solution.data( ) );
    callPardiso( PardisoPhase::CLEANUP, nullptr, nullptr, nullptr, nullptr, nullptr );

    return solution;
}

auto makePardisoSolver( )
{
    return std::function { []( const linalg::AbsSparseMatrix& matrix, const std::vector<double>& rhs )
        { return pardisoSolve2( matrix, rhs, matrix.symmetricHalf( ) ); } };
}

} // namespace mlhp

#endif

